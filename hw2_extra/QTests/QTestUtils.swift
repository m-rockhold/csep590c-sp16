//
//  QTestUtils.swift
//  Q
//
//  Created by Rockhold, Michael on 4/23/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation
import CoreLocation

typealias RawDataRowType = [AnyObject]
//  0       1                                      2      3           4         5           6         7     8        9         10            11      12   13             14      15       16          17          18          19                                20   21    22           23                24           25          26
//[ 25966, "4366EB34-3F80-49EE-B606-C7A67DF9D3BA", 25966, 1459339383, "386118", 1459339383, "386118", null, "94928", "771299", "2016107189", "2606", "1", "FRAUD-CHECK", "2600", "FRAUD", 1459184640, 1456790400, 1459166400, "102XX BLOCK OF LAKE CITY WY NE", "L", "L2", "1100.2015", "-122.301612854", "47.703125", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.703125", "-122.301612854", null, false ] ]

// 26:
//  0                                                               1            2                 3     4
//[ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.703125", "-122.301612854", null, false ] ]

struct CleanDataRowType : Equatable {
    let kind:String
    let desc:String
    let date:NSDate
    let location:CLLocation
    
    static func exemplar() -> CleanDataRowType { return CleanDataRowType(kind:"", desc:"", date:NSDate(), location:CLLocation(latitude: 0,longitude: 0)) }
}

func ==(lhs: CleanDataRowType, rhs: CleanDataRowType) -> Bool {
    return lhs.kind == rhs.kind
        && lhs.desc == rhs.desc
        && lhs.date == rhs.date
        && lhs.location == rhs.location
}

class QTestUtils {
    
    class var raw_data:[RawDataRowType]! {
        let fixtureFileURL = NSBundle(forClass: Problem1Tests.self).URLForResource("raw_data", withExtension: "json")!
        let data = NSData(contentsOfURL: fixtureFileURL)!
        
        do {
            let json:[String:AnyObject] = try NSJSONSerialization.JSONObjectWithData(data, options:.AllowFragments) as! [String:AnyObject]
            return (json["data"] as! [RawDataRowType])
        }
        catch {
            return nil
        }
    }
    
    class var clean_data:[CleanDataRowType]! {
        var out = [CleanDataRowType]()
        for rawRow in QTestUtils.raw_data! {
            do { try out.append(QTestUtils.rawToClean(rawRow)) } catch {}
        }
        return out
    }
    
    class func arraysAreEqual<E:Equatable>(a a:[E], b:[E]) -> Bool {
        
        return a.count == b.count   // TODO: more rigour
    }

    class func rawToClean(rawRow:RawDataRowType) throws -> CleanDataRowType {
        
        let f26 = rawRow[26] as! [AnyObject]
        let latStr = f26[1] as! String
        let longStr = f26[2] as! String
        
        return CleanDataRowType(
            kind: rawRow[13] as! String,
            desc: rawRow[15] as! String,
            date: NSDate(timeIntervalSince1970:(rawRow[16] as! NSNumber).doubleValue),
            location: CLLocation(latitude: Double(latStr)!, longitude: Double(longStr)!))
    }
}