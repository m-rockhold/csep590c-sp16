//
//  Problem1Tests.swift
//  Problem1Tests
//
//  Created by Rockhold, Michael on 4/20/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import XCTest
import CoreLocation
@testable import Q

class Problem1Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func testExecutingQueries() {
        
        let left = AllNode<RawDataRowType>()
        let right = FilterNode<RawDataRowType>(filter: { row in
                let i = (row[0] as! NSNumber).intValue
                return i % 2 != 0
            }
        )
        
        let q = ThenNode<RawDataRowType, RawDataRowType, RawDataRowType>(left:left, right:right)
        do {
            let out = try q.execute(QTestUtils.raw_data!)
            var good = [RawDataRowType]()
            for row in QTestUtils.raw_data! {
                let i = (row[0] as! NSNumber).intValue
                if (i % 2 != 0) {
                    good.append(row)
                }
            }
            
            XCTAssertTrue(QTestUtils.arraysAreEqual(a:out, b:good))
        }
        catch {
            XCTFail()
        }
    }
    
    
    func testWriteAQuery() {
        var good1 = [RawDataRowType]()
        var good2 = [RawDataRowType]()

        for row in QTestUtils.raw_data! {
            if (row[13] as! String).containsString("THEFT") {
                good1.append(row)
            }
            if (row[13] as! String).hasPrefix("VEH-THEFT") {
                good2.append(row)
            }
        }

        let thefts_query = FilterNode<RawDataRowType>(filter: { (row:RawDataRowType) throws -> Bool in
                return (row[13] as! String).containsString("THEFT")
            })

        let auto_thefts_query = FilterNode<RawDataRowType>(filter: { (row:RawDataRowType) throws -> Bool in
                return (row[13] as! String).hasPrefix("VEH-THEFT")
            })
        
        do {
            let out1 = try thefts_query.execute(QTestUtils.raw_data!);
            let out2 = try auto_thefts_query.execute(QTestUtils.raw_data!);
            
            XCTAssertTrue(QTestUtils.arraysAreEqual(a:out1, b:good1))
            XCTAssertTrue(QTestUtils.arraysAreEqual(a:out2, b:good2))
        }
        catch {
            XCTFail()
        }
    }

    
    func testCallChaining() {
        
        let query = AllNode<RawDataRowType>()   // still not as nice as "Q. ...", but I'll take it
            .apply({ row in
                        let i = (row[0] as! NSNumber).intValue
                        return Int(i % 2)
                    })
            .filter({ row in
                        return row != 0
                    })
            .count()
        
        do {
            var out = try query.execute(QTestUtils.raw_data!)
            
            var good = 0
            for row in QTestUtils.raw_data! {
                let i = (row[0] as! NSNumber).intValue
                if (i % 2 != 0) {
                    good = good + 1
                }
            }

            XCTAssertTrue(out.count == 1)
            XCTAssertTrue(out[0] == good)
        }
        catch {
            XCTFail()
        }
    }

    
    func testCleanTheData() {
        let cleanup_query = ApplyNode<RawDataRowType,CleanDataRowType>(applicationFun: QTestUtils.rawToClean)

        do {
            let out = try cleanup_query.execute(QTestUtils.raw_data!)
            XCTAssertTrue(QTestUtils.arraysAreEqual(a:out, b:QTestUtils.clean_data))
        }
        catch {
            XCTFail()
        }
    }

    func testReimplementQueriesWithCallChaining() {
        
        let cleanup_query_2 = AllNode<RawDataRowType>().apply(QTestUtils.rawToClean)
        
        let thefts_query_2 = AllNode<CleanDataRowType>().filter({ e in
            return e.kind.containsString("THEFT")
            });
        
        let auto_thefts_query_2 = thefts_query_2.filter({ e in
            return e.kind.hasPrefix("VEH-");
            })

        
        do {
            let out = try cleanup_query_2.execute(QTestUtils.raw_data!)
            
            XCTAssertTrue(QTestUtils.arraysAreEqual(a:out, b:QTestUtils.clean_data))

            var good1 = [CleanDataRowType]()
            var good2 = [CleanDataRowType]()
            
            for row in QTestUtils.clean_data {
                if row.kind.containsString("THEFT") {
                    good1.append(row)
                }
                if row.kind.hasPrefix("VEH-THEFT") {
                    good2.append(row)
                }
            }
            
            let out1 = try thefts_query_2.execute(QTestUtils.clean_data);
            let out2 = try auto_thefts_query_2.execute(QTestUtils.clean_data);
            XCTAssertTrue(QTestUtils.arraysAreEqual(a:out1, b:good1))
            XCTAssertTrue(QTestUtils.arraysAreEqual(a:out2, b:good2))
        }
        catch {
            XCTFail()
        }
    }

}
