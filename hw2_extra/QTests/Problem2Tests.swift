//
//  Q.swift
//  Q
//
//  Created by Rockhold, Michael on 4/23/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import XCTest
@testable import Q

class Problem2: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testOptimizeFilters() {
        typealias T = CleanDataRowType
        let q = AllNode<T>().filter { row in return true }.filter { row in return true }
        let q2 = q.optimize() as! ThenNode<T,T,T>
        
        XCTAssert(q2.left.dynamicType === AllNode<T>.self, "\(q2.left.dynamicType) should equal \(AllNode<T>.self)")
        XCTAssert(q2.right.dynamicType === FilterNode<T>.self, "\(q2.right.dynamicType) should equal \(FilterNode<T>.self)")
    }
    
    func testInternalNodeTypesAndCountIf() {
        typealias T = RawDataRowType
        let q = AllNode<T>().filter { row in return row.first!.intValue % 2 != 0 }.count()
        let q2 = q.optimize() as! ThenNode<T,T,Int>;

        XCTAssert(q2.left.dynamicType === AllNode<T>.self, "\(q2.left.dynamicType) should equal \(AllNode<T>.self)")
//        XCTAssert(q2.right.dynamicType === CountIf<T>.self, "\(q2.right.dynamicType) should equal \(CountIf<T>.self)")
    }
}
