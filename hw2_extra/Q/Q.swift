//
//  Q.swift
//  Q
//
//  Created by Rockhold, Michael on 4/20/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation

enum ASTError : ErrorType {
    case Unimplemented
    case Other
}

class ASTNode<T,U> {

    typealias InType = T
    typealias OutType = U
    
    func execute(data:[InType]) throws -> [OutType] {
        throw ASTError.Unimplemented
    }
    
    func filter(f:FilterNode<OutType>.FilterType) -> ThenNode<InType, OutType, OutType> {
        return ThenNode(left: self, right: FilterNode(filter:f))
    }
    
    func apply<AppliedOutType>(f:ApplyNode<OutType,AppliedOutType>.ApplicationFunType) -> ThenNode<InType, OutType, AppliedOutType> {
        return ThenNode(left: self, right:ApplyNode(applicationFun:f));
    }
    
    func count() -> ThenNode<InType, OutType, Int> {
        return ThenNode(left:self, right:CountNode<OutType>())
    }
    
    func optimize() -> ASTNode {
        return self
    }
}

class AllNode<T> : ASTNode<T,T> {
    override func execute(data: [InType]) throws -> [OutType] {
        return data
    }
}

class FilterNode<T> : ASTNode<T,T> {
    
    typealias FilterType = (InType) throws -> Bool
    
    let filter: FilterType
    
    init(filter: FilterType) {
        self.filter = filter
        super.init()
    }
    
    override func execute(data: [InType]) throws -> [OutType] {
        return try data.filter(self.filter)
    }
}


class ApplyNode<T,U> : ASTNode<T,U> {
    
    typealias ApplicationFunType = (InType) throws -> OutType
    
    let applicationFun: ApplicationFunType
    
    init(applicationFun: ApplicationFunType) {
        self.applicationFun = applicationFun
        super.init()
    }
    
    override func execute(data: [InType]) throws -> [OutType] {
        return try data.map(self.applicationFun)
    }
}

class CountNode<T> : ASTNode<T,Int> {
    
    override func execute(data: [InType]) throws -> [OutType] {
        return [data.count]
    }
}

class ThenNode<T, U, V> : ASTNode<T,V> {
    
    typealias LeftType = ASTNode<InType, U>
    typealias RightType = ASTNode<U, OutType>
    
    let left: LeftType
    let right: RightType
    
    init(left:LeftType, right:RightType) {
        self.left = left
        self.right = right
        super.init()
    }
    
    override func execute(data: [InType]) throws -> [OutType] {
        return try self.right.execute(self.left.execute(data));
    }
}

struct NodeProduct<A, B> {
    typealias LeftNodeType = A
    typealias RightNodeType = B
    
    let left:LeftNodeType
    let right:RightNodeType
}

class CartesianProductNode<T, U, V> : ASTNode<T, NodeProduct<U,V> > {
  
    typealias LeftType = ASTNode<InType, U>
    typealias RightType = ASTNode<InType, V>
    
    let left: LeftType
    let right: RightType
    
    init(left:LeftType, right:RightType) {
        self.left = left
        self.right = right
        super.init()
    }
    
    override func execute(data: [InType]) throws -> [OutType] {
        
        func joinArrays(leftarray leftarray:[OutType.LeftNodeType], rightarray:[OutType.RightNodeType]) -> [OutType] {
            var cartesianProduct = [OutType]()
            for row in leftarray {
                for column in rightarray {
                    cartesianProduct.append(OutType(left: row, right: column))
                }
            }
            return cartesianProduct;
        }
        
        return try joinArrays(leftarray:self.left.execute(data), rightarray:self.right.execute(data))
    }
}
