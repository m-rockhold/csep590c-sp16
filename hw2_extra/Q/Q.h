//
//  Q.h
//  Q
//
//  Created by Rockhold, Michael on 4/20/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for Q.
FOUNDATION_EXPORT double QVersionNumber;

//! Project version string for Q.
FOUNDATION_EXPORT const unsigned char QVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Q/PublicHeader.h>


