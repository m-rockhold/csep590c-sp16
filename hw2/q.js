/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 * ©2016, Pavel Panchekha and the University of Washington.
 * Released under the MIT license.
 */

//// The AST

// This class represents all AST Nodes.
function ASTNode(type) {
    this.type = type;
}
ASTNode.prototype = {};
ASTNode.prototype.execute = function(table) {
    throw new Error("Unimplemented AST node 'execute' method for " + this.type)
}
ASTNode.prototype.optimize = function() { return this; }
ASTNode.prototype.run = function(data) {
    this.optimize().execute(data);
}

// The All node just outputs all records.
function AllNode() {
    ASTNode.call(this, "All");
}
// This is how we make AllNode subclass from ASTNode.
AllNode.prototype = Object.create(ASTNode.prototype);
AllNode.prototype.execute = function(table) {
    return table;
}

// The Filter node uses a callback to throw out some records.
function FilterNode(callback) {
    ASTNode.call(this, "Filter");
    this.callback = callback;
}
FilterNode.prototype = Object.create(ASTNode.prototype);
FilterNode.prototype.execute = function(table) {
    return table.filter(this.callback);
}

// The Then node chains multiple actions on one data structure.
function ThenNode(first, second) {
    ASTNode.call(this, "Then");
    this.first = first;
    this.second = second;
}
ThenNode.prototype = Object.create(ASTNode.prototype);
ThenNode.prototype.execute = function(table) {
    return this.second.execute(this.first.execute(table));
}
ThenNode.prototype.optimize = function() {
    return new ThenNode(this.first.optimize(), this.second.optimize())
}

// ApplyNode(f): This runs f on every record in the array and collects the results in a new array.
// ApplyNode is essentially a functional map operator.
function ApplyNode(f) {
    ASTNode.call(this, "Apply");
    this.callback = f;
}
ApplyNode.prototype = Object.create(ASTNode.prototype);
ApplyNode.prototype.execute = function(table) {
    return table.map(this.callback);
}

// CountNode(): This returns an array with a single entry, the number of entries in the array.
function CountNode() {
    ASTNode.call(this, "Count");
}
CountNode.prototype = Object.create(ASTNode.prototype);
CountNode.prototype.execute = function(table) {
    return [table.length];
}

//// Internal node types and CountIf

function CountIfNode(callback) {
    ASTNode.call(this, "CountIf");
    this.callback = callback;
}
CountIfNode.prototype = Object.create(ASTNode.prototype);
CountIfNode.prototype.execute = function(table) {
    return [table.filter(this.callback).length];
}

//// Write a query
// Define the `thefts_query` and `auto_thefts_query` variables

var thefts_query = new FilterNode(function(e) {
    return e[13].indexOf("THEFT") != -1;
});

var auto_thefts_query = new FilterNode(function(e) {
    return e[13].startsWith("VEH-THEFT");
});

//// Clean the data

var cleanup_query = new ApplyNode(function (e) {    
    return {
        type: e[13],
        description: e[15],
        date: e[17],
        area: e[18]      
    };
});


//// Implement a call-chaining interface

ASTNode.prototype.filter = function (f) {
    return new ThenNode(this, new FilterNode(f));
}

ASTNode.prototype.apply = function (f) {
    return new ThenNode(this, new ApplyNode(f));
}

ASTNode.prototype.count = function () {
    return new ThenNode(this, new CountNode());
}

Q = Object.create(new AllNode());

//// Reimplement queries with call-chaining

var cleanup_query_2 = Q.apply(function (e) {    
    return {
        type: e[13],
        description: e[15],
        date: e[17],
        area: e[18]      
    };
});

var thefts_query_2 = Q.filter(function(e) {
    return e.type.indexOf("THEFT") != -1;
});

// var auto_thefts_query_2 = Q.filter(function(e) {
//     return e.type.startsWith("VEH-THEFT");
// });

// This doesn't seem simpler than the old auto_thefts_query_2, so this is probably not what you meant....
var auto_thefts_query_2 = thefts_query_2.filter(function(e) {
    return e.type.startsWith("VEH-");
})

function AddOptimization(node_type, f) {
    var old = node_type.prototype.optimize;
    node_type.prototype.optimize = function() {
        var new_this = old.apply(this, arguments);
        return f.apply(new_this, arguments) || new_this;
    }
}

// Add a filter-combining optimization
AddOptimization(ThenNode, function() {
	if (this.first.type == 'Then' && this.second.type == 'Filter' && this.first.second.type == 'Filter' ) {
        var f = this.first.second.callback;
        var g = this.second.callback;
		return new ThenNode(
            this.first.first, new FilterNode(function(e) {
                    return f(e) && g(e);
                })
            );
	}
});

AddOptimization(ThenNode, function() {
	if (this.first.type == 'Then' && this.second.type == 'Count' && this.first.second.type == 'Filter' ) {
		return new ThenNode(this.first.first, new CountIfNode(this.first.second.callback));
	}
});

//// Cartesian Products
function mergeTuple(e) {
    if (Array.isArray(e.right)) {
        return e.right;
    }
    else if (typeof(e.right) == 'object') {
        var combo = {};
        for (p in e.left) {
            combo[p] = e.left[p];
        }
        for (p in e.right) {
            combo[p] = e.right[p];
        }
        return combo;
    }
    else {
        return e.right;
    }
}

function joinArrays(leftarr, rightarr, f) {
    var cartesianProduct = [];
    for (row of leftarr) {
        for (column of rightarr) {
            if (f(row, column)) {
                cartesianProduct.push({left:row, right:column});
            }
        }
    }
    return cartesianProduct;
}

function mergeArray(arr) {
    return arr.map(mergeTuple);
}

function CartesianProductNode(left, right) {
    ASTNode.call(this, "CartesianProduct");
    this.left = left;
    this.right = right;
}
CartesianProductNode.prototype = Object.create(ASTNode.prototype);
CartesianProductNode.prototype.execute = function(table) {
        var leftex = this.left.execute(table);
        var rightex = this.right.execute(table);
        return joinArrays(leftex, rightex, function(left, right){return true;});
    };
CartesianProductNode.prototype.optimize = function() {
    return new CartesianProductNode(this.left.optimize(), this.right.optimize());
}

//// Joins

Q.product = function (left, right) {
    return new CartesianProductNode(left, right);
}

Q.join = function (f, left, right) {
    var f1 = function(e) { return f(e.left, e.right); };
    f1.onField = f.onField;
    return new ThenNode(
                        new ThenNode(
                                    new CartesianProductNode(left, right),
                                    new FilterNode(f1)
                        ),
                        new ApplyNode(mergeTuple)
          );        
}

//// Optimizing joins

function JoinNode(f, left, right) {
    ASTNode.call(this, "Join");
    this.callback = f;
    this.left = left;
    this.right = right;
}
JoinNode.prototype = Object.create(ASTNode.prototype);
JoinNode.prototype.execute = function(table) {
        return mergeArray(joinArrays(this.left.execute(table), this.right.execute(table), this.callback));
    };

AddOptimization(ThenNode, function() {
	if (this.first.type == 'Then'
        && this.first.first.type == 'CartesianProduct'
        && this.first.second.type == 'Filter'
        && this.second.type == 'Apply'
        && this.second.callback == mergeTuple ) {
            
		return new JoinNode(this.first.second.callback,
                            this.first.first.left,
                            this.first.first.right
                            ).optimize();
	}
});

//// Join on fields

Q.on = function(field) {
    var f = function(l, r) { return l[field] == r[field]; };
    f.onField = field; 
    return f
}

function make_hash_table(arr, field) {
    var keys = Array.from(new Set(arr.map(function(e) {
        return e[field];        
    })));
    
    var t = {};
    for (k of keys) {
        t[k] = arr.filter(function(e){
                return e[field] == k;
            })
    }
    return t;
}

//// Implement hash joins

function HashJoinNode(field, left, right) {
    ASTNode.call(this, "HashJoin");
    this.callback = Q.on(field);
    this.left = left;
    this.right = right;
}
HashJoinNode.prototype = Object.create(ASTNode.prototype);
HashJoinNode.prototype.execute = function(table) {
    
        var leftHashTable = make_hash_table(this.left.execute(table), this.callback.onField);
    
        var rightHashTable = make_hash_table(this.right.execute(table), this.callback.onField);

        var keySet = new Set(Object.keys(leftHashTable));
        for (k of Object.keys(rightHashTable)) {
            keySet.add(k);
        }
        var allKeys = Array.from(keySet);
        
        var result = [];
        for (k of allKeys) {
            var l = leftHashTable[k];
            var r = rightHashTable[k];
            
            result = result.concat( mergeArray(joinArrays(l?l:[], r?r:[], this.callback)) );
        }
        return result;
    };


//// Optimize joins on fields to hash joins

JoinNode.prototype.optimize = function() {
	return (this.callback.onField != null)
			? new HashJoinNode(this.callback, this.left, this.right)
			: this;
};
