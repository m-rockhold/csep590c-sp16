//
//  StandardOutput.swift
//  minicypher
//
//  Created by Rockhold, Michael on 8 June 2016.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation

public class StandardOutput {
    public struct StderrOutputStream: OutputStreamType {
        public mutating func write(string: String) {
            fputs(string, stderr)}
    }

    public struct StdoutOutputStream: OutputStreamType {
        public mutating func write(string: String) {
            fputs(string, stdout)}
    }

    public static var errStream = StderrOutputStream()
    public static var outStream = StdoutOutputStream()
}