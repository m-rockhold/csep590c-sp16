%{
#import <Foundation/Foundation.h>
#include "Parser.h"
#define DONT_REINCLUDE_PARSER_INFO_IN_BRIDGING_HEADER 1
#import "minicypher-Swift.h"

#define SCANTEXT nsstring(yyget_text(yyscanner), yyget_leng(yyscanner))

extern Parser* parser(yyscan_t scanner);
extern NSString* nsstring(char* txt, size_t len);

#define create_token(t) { Token* token = [parser(yyscanner) create_ ## t ## _token]; yylval->node = (void*)CFBridgingRetain(token); return (int)token.tokenCode; }

#define create_token_with_value(t,v) { Token* token = [parser(yyscanner) create_ ## t ## _token:v]; yylval->node = (void*)CFBridgingRetain(token); return (int)token.tokenCode; }

%}

%option reentrant bison-bridge bison-locations

%x COMMENT_TO_EOL COMMENTING

%%

"//"                    { [parser(yyscanner) startCommenting]; BEGIN(COMMENT_TO_EOL); }
"/*"                    { [parser(yyscanner) startCommenting]; BEGIN(COMMENTING); }
[ \t]                   ;
[\n]                    ;
"MATCH"                 create_token(MATCH);
"WHERE"                 create_token(WHERE);
"RETURN"                create_token(RETURN);
"true"                  create_token_with_value(BOOLEAN, TRUE);
"false"                 create_token_with_value(BOOLEAN, FALSE);

[a-zA-Z_][a-zA-Z0-9_]*  create_token_with_value(IDENTIFIER, SCANTEXT);
[0-9]+                  create_token_with_value(INTEGER, SCANTEXT);
\"[a-zA-Z0-9_~!@#$%^&*\(\)\{\}\[\]\\\?<>]*\'':;\" create_token_with_value(STRING, SCANTEXT);

"("                     create_token(LEFT_PAREN);
")"                     create_token(RIGHT_PAREN);
"{"                     create_token(LEFT_BRACE);
"}"                     create_token(RIGHT_BRACE);
"["                     create_token(LEFT_SQUARE_BRACKET);
"]"                     create_token(RIGHT_SQUARE_BRACKET);

"<"                     create_token(LESS_THAN);
">"                     create_token(GREATER_THAN);
"."                     create_token(DOT);
","                     create_token(COMMA);
";"                     create_token(SEMICOLON);
":"                     create_token(COLON);
"-"                     create_token(DASH);
"="                     create_token(EQUALS);
"!="                    create_token(NOT_EQUALS);

.                       [parser(yyscanner) reportScanError:SCANTEXT];

<COMMENT_TO_EOL>[^\n]*  { [parser(yyscanner) appendCommentString:SCANTEXT pushing:NO]; }
<COMMENT_TO_EOL>[\n]    { [parser(yyscanner) endCommenting]; BEGIN(INITIAL); }

<COMMENTING>"/*"        { [parser(yyscanner) appendCommentString:SCANTEXT pushing:YES]; }
<COMMENTING>"*/"        {
                            [parser(yyscanner) decrCommentLevel];
                            if ( [parser(yyscanner) topCommentLevel] ) {
                                [parser(yyscanner) endCommenting];
                                BEGIN INITIAL;
                            }
                            [parser(yyscanner) appendCommentString:SCANTEXT pushing:NO];
                        }
<COMMENTING>.           { [parser(yyscanner) appendCommentString:SCANTEXT pushing:NO]; }
<COMMENTING>\n          ;

%%

NSString* nsstring(char* txt, size_t len) {
    return [[NSString alloc] initWithBytes:txt length:len encoding:NSASCIIStringEncoding];
}
