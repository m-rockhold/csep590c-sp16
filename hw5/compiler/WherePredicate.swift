//
//  WherePredicate.swift
//  compiler
//
//  Created by Rockhold, Michael on 6/13/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation

public class WherePredicate : ASTNode {
    
    let identifierToken:IdentifierToken
    let equalityOperator:EqualityOperator
    let stringLiteralToken:StringLiteralToken

    init(identifierToken:IdentifierToken, equalityOperator:EqualityOperator, stringLiteralToken:StringLiteralToken) {
        self.identifierToken = identifierToken
        self.equalityOperator = equalityOperator
        self.stringLiteralToken = stringLiteralToken
        super.init()
    }

    override public func accept(v: Visitor) { v.visit(self) }
}