//
//  main.swift
//  minicypher
//
//  Created by Rockhold, Michael on 8 June 2016.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation

let d = Driver()
do {
    try d.run()
}
    
catch Driver.DriverError.NotEnoughArguments {
    print("not enough arguments")
    exit( 1 )
}
    
catch Driver.DriverError.SyntaxError {
    print("expected -[SDTR] <filename>")
    exit( 1 )
}
    
catch Driver.DriverError.CannotOpen(let sourceFileName) {
    print("could not open file \(sourceFileName)")
    exit( 1 )
}

catch Driver.DriverError.NotYetImplemented(let commandName) {
    print("Not yet implemented command \"\(commandName)\"")
    exit( 1 )
}
    
catch Driver.DriverError.UnknownCommand {
    print("Unknown command; exiting")
    exit( 1 )
}
    
catch Driver.DriverError.InputErrorCount(let errorCount) {
    print("Encountered \(errorCount) errors")
    exit( 1 )
}
    
catch {
    print("Unknown error; exiting")
    exit( 1 )
}


exit(0)
