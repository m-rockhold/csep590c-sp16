//
//  Driver.swift
//  minicypher
//
//  Created by Rockhold, Michael on 8 June 2016.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation

public class Driver {
    public enum DriverError : ErrorType {
        case NotEnoughArguments
        case CannotOpen(filename:String)
        case SyntaxError
        case UnknownCommand
        case TooManyCommands
        case NotYetImplemented(String)
        case InputErrorCount(Int)
    }
    
    public enum DriverCommands {
        case Scan
        case DumpAST
        case DOT
        case Run
    }
    
    class SymbolTable {
        func addBuiltIns() { }
    }
    
    func parseCmdLine(args:[String]) throws -> ([DriverCommands],String) {
        if args.count != 3 {
            throw DriverError.NotEnoughArguments
        }
        
        var commands = [DriverCommands]()
        let fileName = args[2]
        var commandStr = args[1]
        // Parse the command string
        // It starts with '-', and contains a combination of the three
        // letters (S, D, P, T, and R), in no particular order, but the
        // commands passed back to the caller will match the order of
        // the command letters in the argument
        if !commandStr.hasPrefix("-") {
            throw DriverError.SyntaxError
        }
        commandStr.removeAtIndex(commandStr.startIndex)
        for i in (commandStr.startIndex..<commandStr.endIndex) {
            switch commandStr[i] {
            case "S":
                commands.append(.Scan)
            case "D":
                commands.append(.DumpAST)
            case "T":
                commands.append(.DOT)
            case "R":
                commands.append(.Run)
                
            default:
                throw DriverError.UnknownCommand
            }
        }
        return (commands, fileName)
    }
    
    func processFileNamed(filename:String, processor: (UnsafeMutablePointer<FILE>) throws ->Void) throws {
        
        let programFile = fopen(filename.cStringUsingEncoding(NSASCIIStringEncoding)!, "r".cStringUsingEncoding(NSASCIIStringEncoding))
        if programFile == nil {
            throw DriverError.CannotOpen(filename:filename)
        }
        
        defer {
            fclose(programFile)
        }
        
        try processor(programFile)
    }
    
    func run() throws {
        class AugmentedParser : Parser, ListenerContext {
            
            var outputStream:OutputStreamType {
                get { return StandardOutput.outStream }
                set { }
            }
            
            var errorStream:OutputStreamType {
                get { return StandardOutput.errStream }
                set { }
            }
            
            var showComments:Bool = false
            var showLexerMsgs:Bool = false
            
            var symbolTable:SymbolTable
            var goal: ASTNode?
            var visitor:ObserverVisitor? = nil
            var errorMsgCount = 0
            
            override init() {
                self.symbolTable = SymbolTable()
                self.symbolTable.addBuiltIns()
                super.init()
            }
            
            func addListener(listener:Listener) {
                if self.visitor == nil {
                    self.visitor = ObserverVisitor(listenerContext: self)
                }
                self.visitor!.addListener(listener)
            }

            override func accept(goal:ASTNode) {
                // Start visiting the AST by starting at the top
                if self.errorMsgCount == 0 {
                    goal.accept(visitor!)
                }
            }
            
            override func reportScanError(badToken:String) {
                self.reportError("Invalid character(s) \"\(badToken)\"")
            }
            
            override func acceptComment(comment:String) {
                if self.showComments {
                    self.outputStream.write(">>>\(comment)<<<\n")
                }
            }
            
            override func error(errorString:String) {
                self.reportError("Syntax error (\(errorString))")
            }
            
            override func create_IDENTIFIER_token(value:String) -> Token {
                self.reportLexMsg("ID \(value)")
                return IdentifierToken(value: value)
            }
            
            override func create_INTEGER_token(value:String) -> Token {
                self.reportLexMsg("# \(value)")
                return super.create_INTEGER_token(value)
            }
            
            override func create_BOOLEAN_token(value:Bool) -> Token {
                self.reportLexMsg("f \(value)")
                return BooleanLiteralToken(value: value)
            }
            
            override func create_STRING_token(value:String) -> Token {
                self.reportLexMsg("# \(value)")
                return StringLiteralToken(value: value)
            }
            
            //pragma mark
            private func reportError(errorString:String) {
                errorMsgCount += 1
                self.errorStream.write("\(errorString)\n")
            }
            
            private func reportLexMsg(msg:String) {
                if self.showLexerMsgs {
                    self.outputStream.write("<\(msg)>")
                }
            }

            // ListenerContext methods
            func printString(str:String) {
                self.outputStream.write(str)
            }
        }
        
        let command = try self.parseCmdLine(Process.arguments)
        
        let parser = AugmentedParser()
        parser.debug(false)
        
        for cmd in command.0 {
            try self.processFileNamed(command.1) { programFilePtr in
                
                if cmd == .Scan {
                    parser.showComments = true
                    parser.showLexerMsgs = true
                    parser.scanFile(programFilePtr)
                }
                else {
                    switch cmd {
                    case .DumpAST:
                        parser.addListener(DumpASTListener())
                        
//                    case .DOT:
//                        parser.addListener(ASTGraphListener())
//                        
//                    case .Run:
//                        parser.addListener(ExecuteListener(parser))
                    default: break
                    }
                    parser.parseFile(programFilePtr)
                }
                if parser.errorMsgCount > 0 {
                    throw DriverError.InputErrorCount(parser.errorMsgCount)
                }
            }
        }
    }
}
