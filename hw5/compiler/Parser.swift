//
//  Parser.swift
//  minicypher
//
//  Created by Rockhold, Michael on 8 June 2016.
//  Copyright © 2016 Michael Rockhold. All rights reserved.
//

import Foundation

@objc public class Parser : NSObject {
    enum ParserError : ErrorType {
        case OtherError
    }
    
    var yyscan:yyscan_t = yyscan_t(nil)
    
    override public init() {
        super.init()
        yylex_init_extra (unsafeBitCast(self, UnsafeMutablePointer<Void>.self), &(self.yyscan));
        self.debug(true)
    }
    
    deinit {
        yylex_destroy(self.yyscan);
    }
    
    public func scanFile(filePtr: UnsafeMutablePointer<FILE>) {
        yyset_in(filePtr, self.yyscan)
        
        var yylloc_param = YYLTYPE()

        while my_yylex(self.yyscan, &yylloc_param) != 0 {
        }
    }
    
    public func parseFile(filePtr: UnsafeMutablePointer<FILE>) {
        yyset_in(filePtr, self.yyscan);
        
        yyparse(self.yyscan);
    }
    
    // Override
    public func accept(goal:ASTNode) {
//        self.parseHelper?.accept(goal)
    }
    
    // Utilities

    public func debug(on: Bool) {
        if on {
            yyset_debug(1, self.yyscan)
            set_bison_yydebug(1)
        }
        else {
            yyset_debug(0, self.yyscan)
            set_bison_yydebug(0)
        }
    }
    
    public func wrap() -> Int {
        return 1
    }
    
    // Error Reporting
    // Override
    public func reportScanError(badToken:String) {
//        self.parseHelper?.reportError("Invalid character(s) \"\(badToken)\"")
    }
    
    // Comment handling
    
    // Override
    public func acceptComment(comment:String) {
    }
    
    var commentString = ""
    var commentNestingDepth = 0
    
    public func startCommenting() {
        self.commentString = ""
        self.commentNestingDepth = 0
    }
    
    public func endCommenting() {
//        self.parseHelper?.handleComment(self.commentString)
        self.acceptComment(self.commentString)
        self.commentString = ""
    }
    
    public func appendCommentString(s:String, pushing:Bool) {
        self.commentString = self.commentString + s
        if pushing {
            self.incrCommentLevel()
        }
    }
    
    public func topCommentLevel() -> Bool {
        return (self.commentNestingDepth < 0)
    }
    
    public func incrCommentLevel() {
        self.commentNestingDepth = self.commentNestingDepth + 1
    }
    
    public func decrCommentLevel() {
        self.commentNestingDepth = self.commentNestingDepth - 1
    }

    // Parsing
    
    // Override
    public func error(errorString:String) {
//        self.parseHelper?.reportError("Syntax error (\(errorString))")
    }
    
    // Scanning
    
    public func create_MATCH_token() -> Token {
        return MatchToken()
    }

    public func create_WHERE_token() -> Token {
        return WhereToken()
    }
    public func create_RETURN_token() -> Token {
        return ReturnToken()
    }

    public func create_LEFT_PAREN_token() -> Token {
        return LeftParenToken()
    }
    public func create_RIGHT_PAREN_token() -> Token {
        return RightParenToken()
    }

    public func create_LEFT_BRACE_token() -> Token {
        return LeftBraceToken()
    }

    public func create_RIGHT_BRACE_token() -> Token {
        return RightBraceToken()
    }

    public func create_LEFT_SQUARE_BRACKET_token() -> Token {
        return LeftSquareBracketToken()
    }

    public func create_RIGHT_SQUARE_BRACKET_token() -> Token {
        return RightSquareBracketToken()
    }

    public func create_LESS_THAN_token() -> Token {
        return LessThanToken()
    }

    public func create_GREATER_THAN_token() -> Token {
        return GreaterThanToken()
    }
    
    public func create_EQUALS_token() -> Token {
        return EqualsToken()
    }
    
    public func create_NOT_EQUALS_token() -> Token {
        return NotEqualsToken()
    }

    public func create_DOT_token() -> Token {
        return DotToken()
    }

    public func create_COMMA_token() -> Token {
        return CommaToken()
    }

    public func create_SEMICOLON_token() -> Token {
        return SemiColonToken()
    }

    public func create_COLON_token() -> Token {
        return ColonToken()
    }

    public func create_DASH_token() -> Token {
        return DashToken()
    }
    
    // Override, but call this inherited method
    public func create_IDENTIFIER_token(value:String) -> Token {
//        self.parseHelper?.reportLexMsg("ID \(value)")
        return IdentifierToken(value: value)
    }

    // Override, but call this inherited method
    public func create_INTEGER_token(value:String) -> Token {
//        self.parseHelper?.reportLexMsg("# \(value)")
        return IntegerLiteralToken(integerString: value)
    }

    // Override, but call this inherited method
    public func create_BOOLEAN_token(value:Bool) -> Token {
//        self.parseHelper?.reportLexMsg("f \(value)")
        return BooleanLiteralToken(value: value)
    }

    // Override, but call this inherited method
    public func create_STRING_token(value:String) -> Token {
//        self.parseHelper?.reportLexMsg("# \(value)")
        return StringLiteralToken(value: value)
    }
}
