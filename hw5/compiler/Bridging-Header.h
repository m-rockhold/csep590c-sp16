#ifndef yacc_declarations_h
#define yacc_declarations_h

#ifndef DONT_REINCLUDE_PARSER_INFO_IN_BRIDGING_HEADER
#include <stdio.h>

#define YYDEBUG 1
#define YYERROR_VERBOSE 1
typedef struct YYLTYPE
{
    int first_line;
    int first_column;
    int last_line;
    int last_column;
} YYLTYPE;

typedef void* yyscan_t;
typedef size_t yy_size_t;

union YYSTYPE;
#define YY_EXTRA_TYPE void *

int yylex_init_extra (YY_EXTRA_TYPE user_defined,yyscan_t* scanner);

int yylex_destroy (yyscan_t yyscanner );

int yyget_debug (yyscan_t yyscanner );

void yyset_debug (int debug_flag ,yyscan_t yyscanner );

YY_EXTRA_TYPE yyget_extra (yyscan_t yyscanner );

FILE *yyget_in (yyscan_t yyscanner );

void yyset_in  (FILE * in_str ,yyscan_t yyscanner );

FILE *yyget_out (yyscan_t yyscanner );

void yyset_out  (FILE * out_str ,yyscan_t yyscanner );

yy_size_t yyget_leng (yyscan_t yyscanner );

char *yyget_text (yyscan_t yyscanner );

int yyget_lineno (yyscan_t yyscanner );

void yyset_lineno (int line_number ,yyscan_t yyscanner );

union YYSTYPE * yyget_lval (yyscan_t yyscanner );

void yyset_lval (union YYSTYPE * yylval_param ,yyscan_t yyscanner);

int yyparse(yyscan_t scanner);

YYLTYPE *yyget_lloc  (yyscan_t yyscanner);

int yylex (union YYSTYPE * yylval_param, YYLTYPE * yylloc_param ,yyscan_t yyscanner);

int my_yylex(yyscan_t scanner, YYLTYPE* yyloc);

void set_bison_yydebug(int b);

#endif

#endif /* yacc_declarations_h */
