//
//  RelationshipPattern.swift
//  compiler
//
//  Created by Rockhold, Michael on 6/13/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation

public class RelationshipPattern : ASTNode {
    
    override public func accept(v: Visitor) { v.visit(self) }
}