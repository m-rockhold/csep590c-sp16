//
//  File.swift
//  compiler
//
//  Created by Rockhold, Michael on 6/12/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation

public class NodeExpression : ASTNode {
    let nodePattern:NodePattern
    
    init(nodePattern:NodePattern) {
        self.nodePattern = nodePattern
        super.init()
    }
    
    override public func accept(v: Visitor) { v.visit(self) }
}