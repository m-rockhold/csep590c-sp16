//
//  ObserverVisitor.swift
//  compiler
//
//  Created by Rockhold, Michael on 6/14/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation

class ObserverVisitor : Visitor {
    
    var listeners:[Listener] = []
    let listenerContext: ListenerContext
    
    init(listenerContext:ListenerContext) {
        self.listenerContext = listenerContext
    }
    
    func addListener(listener:Listener) {
        listeners.append(listener)
    }
        
    // Visit Terminals
    
    func visit(h:MatchToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:WhereToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:ReturnToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:LeftParenToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:RightParenToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:LeftBraceToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:RightBraceToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:LeftSquareBracketToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:RightSquareBracketToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:DotToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:CommaToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:SemiColonToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:ColonToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:DashToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
        
    func visit(h:EqualsToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:NotEqualsToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:LessThanToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:GreaterThanToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:IdentifierToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:IntegerLiteralToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:BooleanLiteralToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:StringLiteralToken) {
        for listener in listeners { listener.visit(h, listenerContext: self.listenerContext) }
    }
    
    // Visit the non-terminals and accept their children to continue the traversal down the tree
    
    // TODO: visit the children
    func visit(h:MatchExpression) {
        for listener in listeners { listener.enter(h, listenerContext: self.listenerContext) }
        
        h.nodeExpression.accept(self)
        for matchRest in h.matchRestList {
            matchRest.accept(self)
        }

        for listener in listeners { listener.exit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:ReturnExpression) {
        for listener in listeners { listener.enter(h, listenerContext: self.listenerContext) }
        
        h.identifierToken.accept(self)
        
        for listener in listeners { listener.exit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:WhereExpression) {
        for listener in listeners { listener.enter(h, listenerContext: self.listenerContext) }
        
        h.predicate.accept(self)
        
        for listener in listeners { listener.exit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:Query) {
        for listener in listeners { listener.enter(h, listenerContext: self.listenerContext) }
        
        h.matchExpression.accept(self)
        h.whereExpression.accept(self)
        h.returnExpression.accept(self)
        
        for listener in listeners { listener.exit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:NodeExpression) {
        for listener in listeners { listener.enter(h, listenerContext: self.listenerContext) }
        
        h.nodePattern.accept(self)
        
        for listener in listeners { listener.exit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:NodePattern) {
        for listener in listeners { listener.enter(h, listenerContext: self.listenerContext) }
        
        for listener in listeners { listener.exit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:RelationshipExpression) {
        for listener in listeners { listener.enter(h, listenerContext: self.listenerContext) }
        
        h.relationshipPattern.accept(self)
        
        for listener in listeners { listener.exit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:MatchRest) {
        for listener in listeners { listener.enter(h, listenerContext: self.listenerContext) }
        
        h.relationshipExpression.accept(self)
        h.nodeExpression.accept(self)
        
        for listener in listeners { listener.exit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:WherePredicate) {
        for listener in listeners { listener.enter(h, listenerContext: self.listenerContext) }
        
        h.identifierToken.accept(self)
        h.equalityOperator.accept(self)
        h.stringLiteralToken.accept(self)

        for listener in listeners { listener.exit(h, listenerContext: self.listenerContext) }
    }
    
    func visit(h:RelationshipPattern) {
        for listener in listeners { listener.enter(h, listenerContext: self.listenerContext) }
        
        for listener in listeners { listener.exit(h, listenerContext: self.listenerContext) }
    }
}