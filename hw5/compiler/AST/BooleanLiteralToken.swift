//
//  BooleanLiteralToken.swift
//  minicypher
//
//  Created by Michael Rockhold on 6/8/16.
//  Copyright © 2016 Michael Rockhold. All rights reserved.
//

import Foundation

@objc public class BooleanLiteralToken : Token {
    
    public let value: Bool
    
    public init(value: Bool) {
        self.value = value
        super.init()
    }
    
    public convenience init(booleanString:String) {
        var v:Bool = false
        if booleanString.lowercaseString == "true" {
            v = true
        }
        self.init(value:v)
    }
    
    override public func tokenCode() -> Int {
        return TokenCode.BOOLEAN_LITERAL.rawValue
    }
    
    override public func accept(v: Visitor) {
        v.visit(self)
    }
}
