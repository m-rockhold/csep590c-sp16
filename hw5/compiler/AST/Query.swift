//
//  File.swift
//  compiler
//
//  Created by Rockhold, Michael on 6/12/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation

public class Query : ASTNode {
    
    let matchExpression:MatchExpression
    let whereExpression:WhereExpression
    let returnExpression:ReturnExpression
    
    init(matchExpression:MatchExpression, whereExpression:WhereExpression, returnExpression:ReturnExpression) {
        self.matchExpression = matchExpression
        self.whereExpression = whereExpression
        self.returnExpression = returnExpression
        super.init()
    }
        
    override public func accept(v: Visitor) { v.visit(self) }
}