//
//  Listener.swift
//  compiler
//
//  Created by Rockhold, Michael on 6/14/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation

protocol ListenerContext {
    func printString(str:String)
}

protocol Listener {

    // Terminals: enter/exit does not really make sense, just visit
    func visit(node:MatchToken, listenerContext:ListenerContext)

    func visit(node:WhereToken, listenerContext:ListenerContext)

    func visit(node:ReturnToken, listenerContext:ListenerContext)

    func visit(node:LeftParenToken, listenerContext:ListenerContext)

    func visit(node:RightParenToken, listenerContext:ListenerContext)

    func visit(node:LeftBraceToken, listenerContext:ListenerContext)

    func visit(node:RightBraceToken, listenerContext:ListenerContext)

    func visit(node:LeftSquareBracketToken, listenerContext:ListenerContext)

    func visit(node:RightSquareBracketToken, listenerContext:ListenerContext)

    func visit(node:DotToken, listenerContext:ListenerContext)

    func visit(node:CommaToken, listenerContext:ListenerContext)

    func visit(node:SemiColonToken, listenerContext:ListenerContext)

    func visit(node:ColonToken, listenerContext:ListenerContext)

    func visit(node:DashToken, listenerContext:ListenerContext)

    func visit(node:EqualsToken, listenerContext:ListenerContext)

    func visit(node:NotEqualsToken, listenerContext:ListenerContext)

    func visit(node:LessThanToken, listenerContext:ListenerContext)

    func visit(node:GreaterThanToken, listenerContext:ListenerContext)

    func visit(node:IdentifierToken, listenerContext:ListenerContext)

    func visit(node:IntegerLiteralToken, listenerContext:ListenerContext)

    func visit(node:BooleanLiteralToken, listenerContext:ListenerContext)

    func visit(node:StringLiteralToken, listenerContext:ListenerContext)

    // Non-terminals: Visitor calls enter<whatever>, then visits children, then calls exit<whatever>
    
    func enter(node:MatchExpression, listenerContext:ListenerContext)
    func exit(node:MatchExpression, listenerContext:ListenerContext)

    func enter(node:ReturnExpression, listenerContext:ListenerContext)
    func exit(node:ReturnExpression, listenerContext:ListenerContext)

    func enter(node:WhereExpression, listenerContext:ListenerContext)
    func exit(node:WhereExpression, listenerContext:ListenerContext)

    func enter(node:Query, listenerContext:ListenerContext)
    func exit(node:Query, listenerContext:ListenerContext)

    func enter(node:NodeExpression, listenerContext:ListenerContext)
    func exit(node:NodeExpression, listenerContext:ListenerContext)

    func enter(node:NodePattern, listenerContext:ListenerContext)
    func exit(node:NodePattern, listenerContext:ListenerContext)

    func enter(node:RelationshipExpression, listenerContext:ListenerContext)
    func exit(node:RelationshipExpression, listenerContext:ListenerContext)

    func enter(node:MatchRest, listenerContext:ListenerContext)
    func exit(node:MatchRest, listenerContext:ListenerContext)

    func enter(node:WherePredicate, listenerContext:ListenerContext)
    func exit(node:WherePredicate, listenerContext:ListenerContext)

    func enter(node:RelationshipPattern, listenerContext:ListenerContext)
    func exit(node:RelationshipPattern, listenerContext:ListenerContext)
}

// Default for all listener methods is to do nothing, so that real derived classes
// may just implement the methods meaningful to their particular purpose
extension Listener {
    // Terminals: enter/exit does not really make sense, just visit
    func visit(node:MatchToken, listenerContext:ListenerContext) {}
    
    func visit(node:WhereToken, listenerContext:ListenerContext) {}
    
    func visit(node:ReturnToken, listenerContext:ListenerContext) {}
    
    func visit(node:LeftParenToken, listenerContext:ListenerContext) {}
    
    func visit(node:RightParenToken, listenerContext:ListenerContext) {}
    
    func visit(node:LeftBraceToken, listenerContext:ListenerContext) {}
    
    func visit(node:RightBraceToken, listenerContext:ListenerContext) {}
    
    func visit(node:LeftSquareBracketToken, listenerContext:ListenerContext) {}
    
    func visit(node:RightSquareBracketToken, listenerContext:ListenerContext) {}
    
    func visit(node:DotToken, listenerContext:ListenerContext) {}
    
    func visit(node:CommaToken, listenerContext:ListenerContext) {}
    
    func visit(node:SemiColonToken, listenerContext:ListenerContext) {}
    
    func visit(node:ColonToken, listenerContext:ListenerContext) {}
    
    func visit(node:DashToken, listenerContext:ListenerContext) {}
    
    func visit(node:EqualsToken, listenerContext:ListenerContext) {}
    
    func visit(node:NotEqualsToken, listenerContext:ListenerContext) {}
    
    func visit(node:LessThanToken, listenerContext:ListenerContext) {}
    
    func visit(node:GreaterThanToken, listenerContext:ListenerContext) {}
    
    func visit(node:IdentifierToken, listenerContext:ListenerContext) {}
    
    func visit(node:IntegerLiteralToken, listenerContext:ListenerContext) {}
    
    func visit(node:BooleanLiteralToken, listenerContext:ListenerContext) {}
    
    func visit(node:StringLiteralToken, listenerContext:ListenerContext) {}
    
    // Non-terminals: Visitor calls enter<whatever>, then visits children, then calls exit<whatever>
    
    func enter(node:MatchExpression, listenerContext:ListenerContext) {}
    func exit(node:MatchExpression, listenerContext:ListenerContext) {}
    
    func enter(node:ReturnExpression, listenerContext:ListenerContext) {}
    func exit(node:ReturnExpression, listenerContext:ListenerContext) {}
    
    func enter(node:WhereExpression, listenerContext:ListenerContext) {}
    func exit(node:WhereExpression, listenerContext:ListenerContext) {}
    
    func enter(node:Query, listenerContext:ListenerContext) {}
    func exit(node:Query, listenerContext:ListenerContext) {}
    
    func enter(node:NodeExpression, listenerContext:ListenerContext) {}
    func exit(node:NodeExpression, listenerContext:ListenerContext) {}
    
    func enter(node:NodePattern, listenerContext:ListenerContext) {}
    func exit(node:NodePattern, listenerContext:ListenerContext) {}
    
    func enter(node:RelationshipExpression, listenerContext:ListenerContext) {}
    func exit(node:RelationshipExpression, listenerContext:ListenerContext) {}
    
    func enter(node:MatchRest, listenerContext:ListenerContext) {}
    func exit(node:MatchRest, listenerContext:ListenerContext) {}
    
    func enter(node:WherePredicate, listenerContext:ListenerContext) {}
    func exit(node:WherePredicate, listenerContext:ListenerContext) {}
    
    func enter(node:RelationshipPattern, listenerContext:ListenerContext) {}
    func exit(node:RelationshipPattern, listenerContext:ListenerContext) {}
}
