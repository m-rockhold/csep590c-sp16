//
//  File.swift
//  compiler
//
//  Created by Rockhold, Michael on 6/12/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation

public class MatchRest : ASTNode {
    let relationshipExpression:RelationshipExpression
    let nodeExpression:NodeExpression
    
    init(relationshipExpression:RelationshipExpression, nodeExpression:NodeExpression) {
        self.relationshipExpression = relationshipExpression
        self.nodeExpression = nodeExpression
        super.init()
    }
    
    override public func accept(v: Visitor) { v.visit(self) }
}