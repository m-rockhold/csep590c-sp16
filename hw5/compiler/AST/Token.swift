import Foundation


@objc public class Token : ASTNode {

    // Keep these in the same order as the <token> lines in Parser.y
    public enum TokenCode: Int {
        case UNKNOWN = 0

        case IDENTIFIER
        case STRING_LITERAL
        case INTEGER_LITERAL
        case BOOLEAN_LITERAL

        case MATCH
        case WHERE
        case RETURN
        case SEMICOLON
        case COLON
        case LEFT_BRACE
        case RIGHT_BRACE
        case COMMA
        case LESS_THAN
        case GREATER_THAN
        case EQUALS
        case NOT_EQUALS
        case DASH
        case DOT
        case LEFT_PAREN
        case RIGHT_PAREN
        case LEFT_SQUARE_BRACKET
        case RIGHT_SQUARE_BRACKET
    }

    public func tokenCode() -> Int { return TokenCode.UNKNOWN.rawValue }    
}

public class MatchToken : Token {
    override public func tokenCode() -> Int { return TokenCode.MATCH.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class WhereToken : Token {
    override public func tokenCode() -> Int { return TokenCode.WHERE.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class ReturnToken : Token {
    override public func tokenCode() -> Int { return TokenCode.RETURN.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class LeftParenToken : Token {
    override public func tokenCode() -> Int { return TokenCode.LEFT_PAREN.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class RightParenToken : Token {
    override public func tokenCode() -> Int { return TokenCode.RIGHT_PAREN.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class LeftBraceToken : Token {
    override public func tokenCode() -> Int { return TokenCode.LEFT_BRACE.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class RightBraceToken : Token {
    override public func tokenCode() -> Int { return TokenCode.RIGHT_BRACE.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class LeftSquareBracketToken : Token {
    override public func tokenCode() -> Int { return TokenCode.LEFT_SQUARE_BRACKET.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class RightSquareBracketToken : Token {
    override public func tokenCode() -> Int { return TokenCode.RIGHT_SQUARE_BRACKET.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class DotToken : Token {
    override public func tokenCode() -> Int { return TokenCode.DOT.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class CommaToken : Token {
    override public func tokenCode() -> Int { return TokenCode.COMMA.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class SemiColonToken : Token {
    override public func tokenCode() -> Int { return TokenCode.SEMICOLON.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class ColonToken : Token {
    override public func tokenCode() -> Int { return TokenCode.COLON.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class DashToken : Token {
    override public func tokenCode() -> Int { return TokenCode.DASH.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class Operator : Token {
}

public class EqualityOperator : Operator {
}

public class ComparisonOperator : Operator {
}

public class EqualsToken : EqualityOperator {
    override public func tokenCode() -> Int { return TokenCode.EQUALS.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class NotEqualsToken : EqualityOperator {
    override public func tokenCode() -> Int { return TokenCode.NOT_EQUALS.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class LessThanToken : ComparisonOperator {
    override public func tokenCode() -> Int { return TokenCode.LESS_THAN.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}

public class GreaterThanToken : ComparisonOperator {
    override public func tokenCode() -> Int { return TokenCode.GREATER_THAN.rawValue }
    override public func accept(v: Visitor) { v.visit(self) }
}
