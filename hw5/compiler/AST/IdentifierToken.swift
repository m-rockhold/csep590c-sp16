//
//  Identifier.swift
//  minicypher
//
//  Created by Michael Rockhold on 6/8/16.
//  Copyright © 2016 Michael Rockhold. All rights reserved.
//

import Foundation

@objc public class IdentifierToken : Token {
    public let value: String
    
    public init(value:String) {
        self.value = value
        super.init()
    }
    
    override public func tokenCode() -> Int { return TokenCode.IDENTIFIER.rawValue }

    override public func accept(v: Visitor) { v.visit(self) }
}
