import Foundation

@objc public class IntegerLiteralToken : Token {
    public let i: Int
    
    public init(value: Int) {
        i = value
        super.init()
    }
    
    public convenience init(integerString:String) {
        guard let v = Int(integerString) else {
            self.init(value: 0)
            return
        }
        self.init(value: v)
    }
    
    override public func tokenCode() -> Int {
        return TokenCode.INTEGER_LITERAL.rawValue
    }

    override public func accept(v: Visitor) {
        v.visit(self)
    }
}
