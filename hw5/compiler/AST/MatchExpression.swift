//
//  File.swift
//  compiler
//
//  Created by Rockhold, Michael on 6/12/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation

public class MatchExpression : ASTNode {
    let nodeExpression: NodeExpression
    let matchRestList: [MatchRest]
    
    init(nodeExpression:NodeExpression, matchRestList:[MatchRest]) {
        self.nodeExpression = nodeExpression
        self.matchRestList = matchRestList
        super.init()
    }

    override public func accept(v: Visitor) { v.visit(self) }
}