%{
    
#define YYDEBUG 1
#define YYERROR_VERBOSE 1

#import <Foundation/Foundation.h>

#import "Parser.h"
#import "Scanner.h"
#define DONT_REINCLUDE_PARSER_INFO_IN_BRIDGING_HEADER 1
#import "minicypher-Swift.h"

void yyerror (YYLTYPE* pLoc, yyscan_t scanner,char* errString);

Parser* parser(yyscan_t scanner);

#define RETAIN(expansion) (void*)CFBridgingRetain(expansion)
#define RELEASE(symbol) CFBridgingRelease(symbol)
#define CAST(tyype, expression) ((__bridge tyype)(expression))

extern int yydebug;
%}

%pure_parser
%locations
%lex-param {yyscan_t scanner}
%parse-param {yyscan_t scanner}

%union {
    void *node;
}

/* types (well, type) of the terminal nodes             */
/* Appearance here is in increasing order of precedence */
/* Order must match what is found in enum TokenCode in Token.swift */
%token <node> UNKNOWN
%token <node> IDENTIFIER INTEGER_LITERAL BOOLEAN_LITERAL STRING_LITERAL
%token <node> MATCH WHERE RETURN
%token <node> SEMICOLON COLON
%token <node> LEFT_BRACE RIGHT_BRACE
%right <node> COMMA
%left <node> LESS_THAN GREATER_THAN
%left <node> EQUALS NOT_EQUALS
%left <node> DASH DOT LEFT_PAREN RIGHT_PAREN LEFT_SQUARE_BRACKET RIGHT_SQUARE_BRACKET
/* %token <node> BEGIN_COMMENT END_COMMENT COMMENT      */

/* Types of non-terminals                               */
%type <node> query
%type <node> match_expression
%type <node> where_expression
%type <node> opt_where_expression
%type <node> return_expression
%type <node> node_expression
%type <node> relationship_expression

%type <node> predicate

%type <node> node_pattern
%type <node> opt_node_pattern
%type <node> relationship_pattern
%type <node> opt_relationship_pattern

%type <node> match_rest_list
%type <node> match_rest

%type <node> operator
%type <node> equality_operator
%type <node> comparison_operator

%destructor {
//    ASTNode* node = CAST(ASTNode*,$$);
//    RELEASE($$);
} query

%start query

%%

query                :   match_expression opt_where_expression return_expression
                            {
                                MatchExpression* match = CAST(MatchExpression*, $1);
                                WhereExpression* where = CAST(WhereExpression*, $2);
                                ReturnExpression* returnExpression = CAST(ReturnExpression*, $3);
                                ASTNode* query = [[Query alloc] initWithMatchExpression:match whereExpression:where returnExpression:returnExpression];
                                $$ = RETAIN(query);
                                
                                RELEASE($1);
                                RELEASE($2);
                                RELEASE($3);
                                [parser(scanner) accept:query];
                            }
                    ;

match_expression    :   MATCH node_expression match_rest_list
                        {
                            NodeExpression* node = CAST(NodeExpression*, $2);
                            NSArray<MatchRest*>* matchRests = CAST(NSArray<MatchRest*>*, $2);
                            $$ = RETAIN([[MatchExpression alloc] initWithNodeExpression:node matchRestList:matchRests]);
                            
                            RELEASE($1);
                            RELEASE($2);
                            RELEASE($3);
                        }
                    ;
opt_where_expression:   /* empty */         { $$ = RETAIN([[WhereExpression alloc] init]); }
                    |   where_expression    { $$ = $1; }
                    ;

where_expression    :   WHERE predicate
                        {
                            $$ = RETAIN([[WhereExpression alloc] initWithPredicate:CAST(WherePredicate*, $2)]);
                            RELEASE($1);
                            RELEASE($2);
                        }
                    ;

return_expression   :   RETURN IDENTIFIER
                        {
                            $$ = RETAIN([[ReturnExpression alloc] initWithIdentifierToken:CAST(IdentifierToken*,$2)]);
                            RELEASE($1);
                            RELEASE($2);
                        }
                    ;

node_expression     :   LEFT_PAREN opt_node_pattern RIGHT_PAREN
                        {
                            $$ = RETAIN([[NodeExpression alloc] initWithNodePattern:CAST(NodePattern*, $2)]);
                            
                            RELEASE($1);
                            RELEASE($2);
                            RELEASE($3);
                        }
                    ;

match_rest_list     :   /* empty */          { $$ = RETAIN([NSArray<MatchRest*> array]); }
                    |   match_rest match_rest_list
                        {
                            MatchRest* matchRest = CAST(MatchRest*, $1);
                            NSArray* tail = CAST(NSArray<MatchRest*>*, $2);
                            NSMutableArray<MatchRest*>* matchRestList = [NSMutableArray<MatchRest*> arrayWithObject:matchRest];
                            [matchRestList addObjectsFromArray:tail];
                            $$ = RETAIN([matchRestList copy]);
                            
                            RELEASE($1);
                            RELEASE($2);
                        }
                    ;

match_rest          :   relationship_expression node_expression
                        {
                            RelationshipExpression* relationship = CAST(RelationshipExpression*, $1);
                            NodeExpression* nodeExpression = CAST(NodeExpression*, $2);
                            $$ = RETAIN([[MatchRest alloc] initWithRelationshipExpression:relationship nodeExpression:nodeExpression]);
                            
                            RELEASE($1);
                            RELEASE($2);
                        }
                    ;

relationship_expression : DASH opt_relationship_pattern DASH GREATER_THAN
                            {
                                $$ = RETAIN([[RelationshipExpression alloc] initWithRelationshipPattern:CAST(RelationshipPattern*,$2)]);
                                
                                RELEASE($1);
                                RELEASE($2);
                                RELEASE($3);
                                RELEASE($4);
                            }
                    ;

opt_node_pattern    : /* empty */ { $$ = RETAIN([[NodePattern alloc] init]); }
                    |   node_pattern { $$ = $1; }
                    ;

node_pattern        :   LEFT_PAREN IDENTIFIER RIGHT_PAREN { $$ = RETAIN([[NodePattern alloc] init]); } // TODO: implement node patterns
                    ;

opt_relationship_pattern : /* empty */ { $$ = RETAIN([[RelationshipPattern alloc] init]); }
                    |   relationship_pattern { $$ = $1; }
                    ;

relationship_pattern:   LEFT_SQUARE_BRACKET IDENTIFIER RIGHT_SQUARE_BRACKET { $$ = RETAIN([[RelationshipPattern alloc] init]); } // TODO: implement relationship patterns
                    ;

predicate           :   IDENTIFIER equality_operator STRING_LITERAL
                        {
                            $$ = RETAIN([[WherePredicate alloc] initWithIdentifierToken:CAST(IdentifierToken*,$1)
                                                                       equalityOperator:CAST(EqualityOperator*,$2)
                                                                     stringLiteralToken:CAST(StringLiteralToken*,$3)]);
                            RELEASE($1);
                            RELEASE($2);
                            RELEASE($3);
                        }
/*                    |   IDENTIFIER rel_op INTEGER_LITERAL
                        {
                            $$ = RETAIN([[WherePredicate alloc] initWithIdentifierToken:CAST(IdentifierToken*,$1) operator:CAST(Operator*,$2), integerLiteralToken:CAST(IntegerLiteralToken*,$3)]);
                            RELEASE($1);
                            RELEASE($2);
                            RELEASE($3);
                        } */
                    ;

operator            :   equality_operator   { $$ = $1; }
                    |   comparison_operator { $$ = $1; }
                    ;

comparison_operator :   GREATER_THAN        { $$ = $1; }
                    |   LESS_THAN           { $$ = $1; }
                    ;

equality_operator   :   EQUALS              { $$ = $1; }
                    |   NOT_EQUALS          { $$ = $1; }
                    ;
%%

int my_yylex(yyscan_t scanner, YYLTYPE* yyloc) {
    YYSTYPE yylval_param;
    return yylex(&yylval_param, yyloc, scanner);
}

void set_bison_yydebug(int b) {
    yydebug = b;
}

Parser* parser(yyscan_t scanner) {
    return (__bridge Parser*)yyget_extra(scanner);
}

void yyerror (YYLTYPE* pLoc, yyscan_t scanner, char* errString) {
    [parser(scanner) error:[NSString stringWithCString:errString encoding:NSASCIIStringEncoding]];
}

int yywrap (yyscan_t scanner ) {
    return (int)[parser(scanner) wrap];
}
