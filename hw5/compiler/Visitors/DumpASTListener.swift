import Foundation

public class DumpASTListener : Listener {

    var indentLevel = 0
    
    
    private func indent(listenerContext:ListenerContext) {
        for _ in 0..<indentLevel {
            listenerContext.printString("    ")
        }
    }
    
    // Terminals: enter/exit does not really make sense, just visit
    func visit(node:MatchToken, listenerContext:ListenerContext) {
        listenerContext.printString("match ")
    }
    
    func visit(node:WhereToken, listenerContext:ListenerContext) {
        listenerContext.printString("where ")
    }
    
    func visit(node:ReturnToken, listenerContext:ListenerContext) {
        listenerContext.printString("return ")
    }
    
    func visit(node:LeftParenToken, listenerContext:ListenerContext) {
        listenerContext.printString("(")
    }
    
    func visit(node:RightParenToken, listenerContext:ListenerContext) {
        listenerContext.printString(")")
    }
    
    func visit(node:LeftBraceToken, listenerContext:ListenerContext) {
        listenerContext.printString("{")
    }
    
    func visit(node:RightBraceToken, listenerContext:ListenerContext) {
        listenerContext.printString("}")
    }
    
    func visit(node:LeftSquareBracketToken, listenerContext:ListenerContext) {
        listenerContext.printString("[")
    }
    
    func visit(node:RightSquareBracketToken, listenerContext:ListenerContext) {
        listenerContext.printString("]")
    }
    
    func visit(node:DotToken, listenerContext:ListenerContext) {
        listenerContext.printString("DOT ")
    }
    
    func visit(node:CommaToken, listenerContext:ListenerContext) {
        listenerContext.printString("COMMA ")
    }
    
    func visit(node:SemiColonToken, listenerContext:ListenerContext) {
        listenerContext.printString(";")
    }
    
    func visit(node:ColonToken, listenerContext:ListenerContext) {
        listenerContext.printString(":")
    }
    
    func visit(node:DashToken, listenerContext:ListenerContext) {
        listenerContext.printString("-")
    }
        
    func visit(node:EqualsToken, listenerContext:ListenerContext) {
        listenerContext.printString("==")
    }
    
    func visit(node:NotEqualsToken, listenerContext:ListenerContext) {
        listenerContext.printString("!=")
    }
    
    func visit(node:LessThanToken, listenerContext:ListenerContext) {
        listenerContext.printString("<")
    }
    
    func visit(node:GreaterThanToken, listenerContext:ListenerContext) {
        listenerContext.printString(">")
    }
    
    func visit(node:IdentifierToken, listenerContext:ListenerContext) {
        listenerContext.printString("ID(\(node.value))")
    }
    
    func visit(node:IntegerLiteralToken, listenerContext:ListenerContext) {
        listenerContext.printString("INT(\(node.i))")
    }
    
    func visit(node:BooleanLiteralToken, listenerContext:ListenerContext) {
        listenerContext.printString("BOOL(\(node.value))")
    }
    
    func visit(node:StringLiteralToken, listenerContext:ListenerContext) {
        listenerContext.printString("STR(\(node.value))")
    }
    
    // Non-terminals: Visitor calls enter<whatever>, then visits children, then calls exit<whatever>
    
    func enter(node:MatchExpression, listenerContext:ListenerContext) {
        listenerContext.printString("<MATCHEXP>")
    }
    func exit(node:MatchExpression, listenerContext:ListenerContext) {
        listenerContext.printString("</MATCHEXP>\n")
    }

    func enter(node:ReturnExpression, listenerContext:ListenerContext) {
        listenerContext.printString("<RETURNEXP>")
    }
    func exit(node:ReturnExpression, listenerContext:ListenerContext) {
        listenerContext.printString("</RETURNEXP>\n")
    }

    func enter(node:WhereExpression, listenerContext:ListenerContext) {
        listenerContext.printString("<WHEREEXP>")
    }
    func exit(node:WhereExpression, listenerContext:ListenerContext) {
        listenerContext.printString("</WHEREEXP>\n")
    }
    
    func enter(node:Query, listenerContext:ListenerContext) {
        listenerContext.printString("<WHEREEXP>")
    }
    func exit(node:Query, listenerContext:ListenerContext) {
        listenerContext.printString("</WHEREEXP>\n")
    }
    
    func enter(node:NodeExpression, listenerContext:ListenerContext) {
        listenerContext.printString("<NODEEXP>")
    }
    func exit(node:NodeExpression, listenerContext:ListenerContext) {
        listenerContext.printString("</NODEEXP>\n")
    }
    
    func enter(node:RelationshipExpression, listenerContext:ListenerContext) {
        listenerContext.printString("<RELATIONSHIPEXP>")
    }
    func exit(node:RelationshipExpression, listenerContext:ListenerContext) {
        listenerContext.printString("</RELATIONSHIPEXP>\n")
    }
    
    func enter(node:MatchRest, listenerContext:ListenerContext) {
        listenerContext.printString("<MATCHREST>")
    }
    func exit(node:MatchRest, listenerContext:ListenerContext) {
        listenerContext.printString("</MATCHREST>\n")
    }
    
    func enter(node:WherePredicate, listenerContext:ListenerContext) {
        listenerContext.printString("<WHEREPREDICATE>")
    }
    func exit(node:WherePredicate, listenerContext:ListenerContext) {
        listenerContext.printString("</WHEREPREDICATE>\n")
    }
    
    func enter(node:RelationshipPattern, listenerContext:ListenerContext) {
        listenerContext.printString("<RELATIONSHIPPATTERN>")
    }
    func exit(node:RelationshipPattern, listenerContext:ListenerContext) {
        listenerContext.printString("</RELATIONSHIPPATTERN>\n")
    }
}
