//
//  Article.h
//  extract_dblp
//
//  Created by Rockhold, Michael on 6/4/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Article : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Article+CoreDataProperties.h"
