//
//  Article+CoreDataProperties.h
//  extract_dblp
//
//  Created by Rockhold, Michael on 6/4/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Article.h"

NS_ASSUME_NONNULL_BEGIN

@class Author;

@interface Article (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSSet<Author *> *authors;

@end

@interface Article (CoreDataGeneratedAccessors)

- (void)addAuthorsObject:(Author *)value;
- (void)removeAuthorsObject:(Author *)value;
- (void)addAuthors:(NSSet<Author *> *)values;
- (void)removeAuthors:(NSSet<Author *> *)values;

@end

NS_ASSUME_NONNULL_END
