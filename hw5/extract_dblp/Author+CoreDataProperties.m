//
//  Author+CoreDataProperties.m
//  extract_dblp
//
//  Created by Rockhold, Michael on 6/4/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Author+CoreDataProperties.h"

@implementation Author (CoreDataProperties)

@dynamic name;
@dynamic articles;

@end
