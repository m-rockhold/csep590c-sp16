//
//  Store.h
//  dblpDB
//
//  Created by Rockhold, Michael on 6/4/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol ErrorPresentor <NSObject>
-(void)presentError:(NSError*)error;
@end

@class Article, Author;

@interface Store : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (instancetype)initWithErrorPresentor:(id<ErrorPresentor>)errorPresentor destinationDirectory:(NSURL*)destinationDir;
- (void)save;

- (Author*)findOrCreateAuthorWithName:(NSString*)name;
- (Article*)createNewArticleWithTitle:(NSString*)title;

@end

