//
//  main.m
//  extract_dblp
//
//  Created by Rockhold, Michael on 6/2/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Store.h"
#import "Author+CoreDataProperties.h"
#import "Article+CoreDataProperties.h"

@interface NSString (escape)
- (NSString*)stringByEncodingHTMLEntities;
@end

@implementation NSString (escape)
- (NSString*)stringByEncodingHTMLEntities {
    // Rather then mapping each individual entity and checking if it needs to be replaced, we simply replace every character with the numeric entity
    
    NSMutableString *resultString = [NSMutableString string];
    for(int pos = 0; pos<[self length]; pos++)
        [resultString appendFormat:@"&#%d;",[self characterAtIndex:pos]];
    return [NSString stringWithString:resultString];
}
@end


@interface DBLPElement : NSObject
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSMutableString* content;
@property (nonatomic, strong) NSDictionary<NSString *,NSString *> * attributes;
@property (nonatomic, strong) DBLPElement* parent;

- (instancetype)initWithName:(NSString*)name attributes:(NSDictionary<NSString *,NSString *> *)attributes parent:(DBLPElement*)parent;
@end

@implementation DBLPElement

- (instancetype)initWithName:(NSString*)name attributes:(NSDictionary<NSString *,NSString *> *)attributes parent:(DBLPElement*)parent {
    self = [super init];
    if (self) {
        _name = name;
        _attributes = attributes;
        _parent = parent;
        _content = [NSMutableString string];
    }
    return self;
}

- (void)save:(Store*)store {
}

- (void)finish:(Store*)store {
}

@end

@class AuthorElement;
int articleIdx = 0;

@interface ArticleElement : DBLPElement
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSMutableArray<NSString *>* authorNames;
@end

@interface AuthorElement : DBLPElement
- (instancetype)initWithAttributes:(NSDictionary<NSString *,NSString *> *)attributes parent:(DBLPElement*)parent;
@end

@interface TitleElement : DBLPElement
- (instancetype)initWithAttributes:(NSDictionary<NSString *,NSString *> *)attributes parent:(DBLPElement*)parent;
@end

// ====
@implementation ArticleElement
- (instancetype)initWithAttributes:(NSDictionary<NSString *,NSString *> *)attributes parent:(DBLPElement*)parent {
    self = [super initWithName:@"article" attributes:attributes parent:parent];
    if (self) {
        _authorNames = [NSMutableArray array];
        _title = nil;
    }
    return self;
}

- (void)finish:(Store*)store {
    Article* article = [store createNewArticleWithTitle:self.title];
    for (NSString* authorName in self.authorNames) {
        Author* author = [store findOrCreateAuthorWithName:authorName];
        [article addAuthorsObject:author];
        [author addArticlesObject:article];
    }
    
    NSLog(@"Saving \"%@\"", self.title);
    [store save];
}

@end

@implementation AuthorElement
- (instancetype)initWithAttributes:(NSDictionary<NSString *,NSString *> *)attributes parent:(DBLPElement*)parent {
    return [self initWithName:@"author" attributes:attributes parent:parent];
}


- (void)finish:(Store*)store {
    ArticleElement* parentArticle = (ArticleElement*)(self.parent);
    [parentArticle.authorNames addObject:self.content];
}

@end

@implementation TitleElement
- (instancetype)initWithAttributes:(NSDictionary<NSString *,NSString *> *)attributes parent:(DBLPElement*)parent {
    return [self initWithName:@"title" attributes:attributes parent:parent];
}

- (void)finish:(Store*)store {
    ArticleElement* parentArticle = (ArticleElement*)(self.parent);
    parentArticle.title = [self.content copy];
}

@end


@interface DBLPParserHelper : NSObject <NSXMLParserDelegate, ErrorPresentor>
@property (nonatomic, strong) DBLPElement* currentElement;
@property (nonatomic, strong) Store* store;
@end

@implementation DBLPParserHelper

- (instancetype)init {
    self = [super init];
    if (self) {
        _store = [[Store alloc] initWithErrorPresentor:self destinationDirectory:[DBLPParserHelper destinationDirectory]];
    }
    return self;
}

+ (NSURL *)destinationDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "dblpDB" in the user's Application Support directory.
    NSURL *appSupportURL = [[[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    return [appSupportURL URLByAppendingPathComponent:@"dblpDB"];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    if ([elementName isEqualToString:@"article"]) {
        self.currentElement = [[ArticleElement alloc] initWithAttributes:attributeDict parent:self.currentElement];
    }
    else if ([elementName isEqualToString:@"author"]) {
        self.currentElement = [[AuthorElement alloc] initWithAttributes:attributeDict parent:self.currentElement];
    }
    else if ([elementName isEqualToString:@"title"]) {
        self.currentElement = [[TitleElement alloc] initWithAttributes:attributeDict parent:self.currentElement];
    }
    else {
        self.currentElement = [[DBLPElement alloc] initWithName:elementName attributes:attributeDict parent:self.currentElement];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {

    [self.currentElement finish:self.store];
    self.currentElement = self.currentElement.parent;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    [self.currentElement.content appendString:string];
}

//- (nullable NSData *)parser:(NSXMLParser *)parser resolveExternalEntityName:(NSString *)name systemID:(nullable NSString *)systemID {
//    
//    NSURL *dtdURL = [NSURL fileURLWithPath:@"/Users/x0ab/Downloads/dblp.dtd"];
//    NSXMLDTD *dtd = [[NSXMLDTD alloc] initWithContentsOfURL:dtdURL options:0 error:nil];
//    NSXMLDTDNode *dtdNode = [dtd entityDeclarationForName:name];
//    
//    NSString *escapedString = [dtdNode.objectValue stringByEncodingHTMLEntities];
//    return [escapedString dataUsingEncoding:NSUTF8StringEncoding];
//}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    [self.store save];
    [self presentError:parseError];
}

- (void)presentError:(NSError *)error {
    NSLog(@"Store error: %@", error);
}

@end

int main(int argc, const char * argv[]) {
    
    @autoreleasepool {
        
        DBLPParserHelper* parserHelper = [DBLPParserHelper new];
        
        NSURL *xmlURL = [NSURL fileURLWithPath:@"/Users/x0ab/Downloads/dblp.xml"];
        NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:xmlURL];
        [parser setDelegate:parserHelper];
        parser.externalEntityResolvingPolicy = NSXMLParserResolveExternalEntitiesAlways;
        [parser setShouldResolveExternalEntities:NO];
        [parser parse]; // return value not used
    }

    return 0;
}
