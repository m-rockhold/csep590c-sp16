//
//  Store.m
//  dblpDB
//
//  Created by Rockhold, Michael on 6/4/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

#import "Store.h"
#import "Author.h"
#import "Article.h"

@interface Store ()

@property (readwrite, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readwrite, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readwrite, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, weak) id<ErrorPresentor> errorPresentor;
@property (nonatomic, strong) NSURL* destinationDirectory;

@end

@implementation Store

#pragma mark - Core Data stack

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize managedObjectContext = _managedObjectContext;

- (instancetype)initWithErrorPresentor:(id<ErrorPresentor>)errorPresentor destinationDirectory:(NSURL*)destinationDir {
    self = [super init];
    if (self) {
        _errorPresentor = errorPresentor;
        _destinationDirectory = destinationDir;
    }
    return self;
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel) {
        return _managedObjectModel;
    }
	
    NSString* execDirPath = [[[[NSProcessInfo processInfo] arguments] objectAtIndex:0] stringByDeletingLastPathComponent];
    NSURL* modelURL = [NSURL URLWithString:@"dblpDB.momd" relativeToURL:[NSURL fileURLWithPath:execDirPath]];
    
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. (The directory for the store is created, if necessary.)
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL shouldFail = NO;
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    // Make sure the application files directory is there
    NSDictionary *properties = [[self destinationDirectory] resourceValuesForKeys:@[NSURLIsDirectoryKey] error:&error];
    if (properties) {
        if (![properties[NSURLIsDirectoryKey] boolValue]) {
            failureReason = [NSString stringWithFormat:@"Expected a folder to store application data, found a file (%@).", [self.destinationDirectory path]];
            shouldFail = YES;
        }
    } else if ([error code] == NSFileReadNoSuchFileError) {
        error = nil;
        [fileManager createDirectoryAtPath:[self.destinationDirectory path] withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    if (!shouldFail && !error) {
        NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        NSURL *url = [self.destinationDirectory URLByAppendingPathComponent:@"OSXCoreDataObjC.storedata"];
        NSLog(@"Creating data store at %@", url);
        if (![coordinator addPersistentStoreWithType:NSXMLStoreType configuration:nil URL:url options:nil error:&error]) {
            coordinator = nil;
        }
        _persistentStoreCoordinator = coordinator;
    }
    
    if (shouldFail || error) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        if (error) {
            dict[NSUnderlyingErrorKey] = error;
        }
        error = [NSError errorWithDomain:@"org.appel-rockhold.dblp" code:9999 userInfo:dict];
        [self.errorPresentor presentError:error];
    }
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];

    return _managedObjectContext;
}

#pragma mark - Core Data Saving and Undo support

- (void)save {
    // Performs the save action for the application, which is to send the save: message to the application's managed object context.
    if (![[self managedObjectContext] commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing before saving", [self class], NSStringFromSelector(_cmd));
    }
    
    NSError *error = nil;
    if ([[self managedObjectContext] hasChanges] && ![[self managedObjectContext] save:&error]) {
        [self.errorPresentor presentError:error];
    }
}

#pragma mark - Accessing DB entities

- (Author*)findOrCreateAuthorWithName:(NSString*)name {
    
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"Author"];
    request.predicate = [NSPredicate predicateWithFormat:@"name LIKE %@", name];
    NSArray<Author*>* authors = [self.managedObjectContext executeFetchRequest:request error:nil];
    if (!authors || authors.count == 0) {
        Author* author = [NSEntityDescription insertNewObjectForEntityForName:@"Author" inManagedObjectContext:self.managedObjectContext];
        author.name = name;
        return author;
    }
    else {
        return authors[0];
    }
}

- (Article*)createNewArticleWithTitle:(NSString*)title {
    
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"Article"];
    request.predicate = [NSPredicate predicateWithFormat:@"title LIKE %@", title];
    NSArray<Article*>* articles = [self.managedObjectContext executeFetchRequest:request error:nil];
    if (!articles || articles.count == 0) {
        Article* article = [NSEntityDescription insertNewObjectForEntityForName:@"Article" inManagedObjectContext:self.managedObjectContext];
        article.title = title;
        return article;
    }
    else {
        return articles[0];
    }

//    Article* article = [NSEntityDescription insertNewObjectForEntityForName:@"Article" inManagedObjectContext:self.managedObjectContext];
//    article.title = title;
//    NSError* saveError = nil;
//    @try {
//        [self.managedObjectContext save:&saveError];
//        return article;
//        
//    } @catch (NSException *exception) {
//        
//        [self.managedObjectContext reset];
//        return nil;
//    }
}


@end
