
function Stream() {
    this.callbacks = []
}

Stream.prototype._push = function(obj) {
    for (callback of this.callbacks) {
        callback(obj);
    }    
}
Stream.prototype.subscribe = function(callback) {
    this.callbacks.push(callback);
    return this;
}
Stream.prototype.unsubscribe = function(callback) {
    var i = this.callbacks.indexOf(callback);
    if (i > -1) {
        this.callbacks.splice(i, 1);
    }
}
Stream.prototype._push_many = function(many) {
    for (obj of many) {
        this._push(obj);
    }
}
Stream.prototype.first = function() {
    var src = this;
    var newStream = new Stream();
    this.subscribe(f = function(obj) {
        src.unsubscribe(f)
        newStream._push(obj);
    });
    return newStream;
}
Stream.prototype.map = function(f) {
    var newStream = new Stream();
    this.subscribe(function(obj){
        newStream._push(f(obj));
    })
    return newStream;
}
Stream.prototype.filter = function(f) {
    var newStream = new Stream();
    this.subscribe(function(obj) {
        if (f(obj)) {
            newStream._push(obj);
        }
    });
    return newStream;
}
Stream.prototype.flatten = function() {
    var newStream = new Stream();
    this.subscribe(function(obj) {
        if (Array.isArray(obj)) {
            obj.forEach(function(element) {
                newStream._push(element);
            });
        }
        else {
            newStream._push(obj);
        }
    });
    return newStream;
}
Stream.prototype.join = function(b) {
    var a = this;
    var newStream = new Stream();
    a.subscribe(function(obj) {
        newStream._push(obj);
    });
    b.subscribe(function(obj) {
        newStream._push(obj);
    });
    return newStream;
}
Stream.prototype.combine = function() {
    var newStream = new Stream();
    this.subscribe(function(stream) {
        stream.subscribe(function(obj){
            newStream._push(obj);
        })
    });
    return newStream;    
}

Stream.prototype.explode = function() {
    var newStream = new Stream();
    this.subscribe(function(obj) {
        if (Array.isArray(obj)) {
            obj.forEach(function(component) {
                newStream._push(component);
            });
        }
    });
    return newStream;    
}

// 1  -1  0  5  1
// 2   3  2
// 2, -2, 0, 10, 15, 3, 2     
Stream.prototype.zip = function(b, f) {
    var a = this;
    var aobj = null;
    var bobj = null;
    var newStream = new Stream();
    a.subscribe(function(obj){
        aobj = obj;
        if (bobj != null) {
            newStream._push(f(aobj, bobj));
        }
    });
    b.subscribe(function(obj){
        bobj = obj;
        if (aobj != null) {
            newStream._push(f(aobj, bobj));
        }
    });
    return newStream;    
}

Stream.timer = function(n) {
    var s = new Stream();
    setInterval(function(){
       s._push(new Date()); 
    }, n);
    s._push(new Date()); 
    return s;
}
Stream.dom = function(element, eventname) {
    var s = new Stream();
    element.on(eventname, function(event) {
        s._push(event);        
    });
    return s;
}
Stream.prototype.throttle = function(millis) {
    var newStream = new Stream();
    var lastEmission = null;
    var waitingObj = null;
    var lastTimeoutID = null;
    
    function emit(obj) {
        newStream._push(obj);
        lastEmission = Date.now();
        if (lastTimeoutID) {
            clearTimeout(lastTimeoutID);
            lastTimeoutID = null;
        }
        lastTimeoutID = setTimeout(function (){
            if (waitingObj) {
                newStream._push(waitingObj);
                waitingObj = null;
                lastTimeoutID = null;
                lastEmission = Date.now();
            }
        }, millis);
    }
    this.subscribe(function(event) {
        if (lastEmission == null) {
            emit(event);
        }
        else {
            if (Date.now()-lastEmission > millis) {
                emit(event);
            }
            else {
                waitingObj = event; // just drop any waitingObj already in hand
            }
        }
    });
    return newStream;    
}
Stream.url = function(url) {
    var s = new Stream();
    $.get(url, function(response){
        s._push(response);
    }, "json");
    return s;
}
Stream.prototype.latest = function() {
    var newStream = new Stream();
    var previousStream = null;
    var self = this;
    this.subscribe(s = function(stream) {
        stream.subscribe(function(obj){
            newStream._push(obj);
            if (previousStream && s != previousStream) {
            	self.unsubscribe(previousStream);
            	previousStream = null;
            }
        });
        previousStream = s;
    });
    return newStream;
}
Stream.query = function(s, getFn) {
    var stream = new Stream();
    getFn(s, function(response){
        stream._push(response);
    });
    return stream;
}
Stream.prototype.enstreamQuery = function(getFn) {
    var newStream = new Stream();
    this.subscribe(function(event) {
        newStream._push(new Stream.query(event.currentTarget.value, getFn));
    });
    return newStream;    
}
Stream.prototype.unique = function(f) {
    var seen = [];
    var newStream = new Stream();
    this.subscribe(function(value){
        var h = f(value);
        if (seen.indexOf(h) == -1) {
            newStream._push(value);
            seen.push(h);
        }
    });
    return newStream;
}
var FIRE911URL = "https://data.seattle.gov/views/kzjm-xkqj/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=10&meta=false&$order=:id";

window.WIKICALLBACKS = {}

function WIKIPEDIAGET(s, cb) {
    $.ajax({
        url: "https://en.wikipedia.org/w/api.php",
        dataType: "jsonp",
        jsonp: "callback",
        data: {
            action: "opensearch",
            search: s,
            namespace: 0,
            limit: 10,
        },
        success: function(data) {cb(data[1])},
    })
}

function appendToList(elem,txt) {
    $(elem).append($("<li></li>").text(txt)); 
}

$(function() {
    var clickCount = 0;
    $("#clicks").text(clickCount);
    
    new Stream.timer()
    .subscribe(function(t) {
        $("#time").text(t);
    });

    new Stream.dom($("#button"), "click")
    .subscribe(function(event){
        $("#clicks").text(++clickCount);
    });

    new Stream.dom($("#mousemove"), "mousemove")
    .throttle(1000)
    .subscribe(function(event){
        var coords = "(" + event.pageX + ", " + event.pageY + ")" 
        $("#mouseposition").text(coords);
    });
    
    function uniquer(value) {
        return value.id;
    }
    
    var allAddresses = [];
    var currentFilterFn = function(address) {
        return true;
    }
    
    new Stream
    .timer(10000)
    .map(function(date){
        return new Stream.url(FIRE911URL);
    })
    .latest()
    .explode()
    .unique(uniquer)
    .map(function(fireEvent){
        return fireEvent["3479077"];
    })
    .subscribe(function(address){
        allAddresses.push(address);
    })
    .filter(function(address){
        return currentFilterFn(address);
    })
    .subscribe(function(address){
        appendToList("#fireevents", address);
    })
        
    new Stream.dom($("#firesearch"), "input")
    .subscribe(function(event){
        currentFilterFn = (event.target.value && event.target.value.length > 0)
            ? function(address) {
                return address.toLowerCase().includes(event.target.value.toLowerCase());
            }
            : function(address) {
                return true;
            }; 

        $("#fireevents").empty();
        allAddresses.filter(function(address){
            return currentFilterFn(address);
        }).forEach(function(address){
            appendToList("#fireevents", address);
        });
    });
        
    // WikipediaSearch Varieties
    // original:
    new Stream.dom($("#wikipediasearch"), "input")
    .throttle(1000)
    .enstreamQuery(WIKIPEDIAGET)
    .latest()
    .subscribe(function(suggestions) {
        $("#wikipediasuggestions").empty();
    })
    .explode()
    .subscribe(function(suggestion) {
    	appendToList("#wikipediasuggestions", suggestion);
    });
    
    // Final "Food for Thought" problem:
    
    // Possible approach to optimization: As we observed in the previous assignment,
    // some interesting opportunities for optimisation appear in the way two or more
    // operations happen to be adjacent: sometimes those can be replaced with a single
    // operation that does the equivalent jobs, but capitalises on the ways we can
    // specialise for that particular combination. In this case, we observe that
    // 'enstreamQuery()' is followed immediately by 'latest()'. Perhaps we can replace that
    // sequence whenever it appears with a new operation, 'latestQuery()'? The
    // efficiency from having slightly fewer moving parts might turn out to be valuable.
    
    // Optimised variant 1:
    Stream.prototype.latestQuery = function(queryFn) {
    	var streamOfQueryResults = new Stream();
    	
		var self = this;
		var queryCallback = null;
		// The 'queryEvent' is the input event from the text box the user types into
		this.subscribe(s = function(queryEvent) {
			// Execute the query and subscribe to its stream of query results
			var queryStream = new Stream.query(queryEvent.currentTarget.value, queryFn);
			// if this was not the first queryEvent to arrive, stop tracking the previous 
			// ones; and remember this one for when its time comes
			if (queryCallback) {
				queryStream.unsubscribe(queryCallback);
			}
			queryCallback = function(obj){
				streamOfQueryResults._push(obj);
			};
			queryStream.subscribe(queryCallback);
		});
		return streamOfQueryResults;
	}
	
    // first 'optimised' variant:
    new Stream.dom($("#wikipediasearch"), "input")
    .throttle(1000)
    .latestQuery(WIKIPEDIAGET)
    .subscribe(function(suggestions) {
        $("#wikipediasuggestionsOpt1").empty();
    })
    .explode()
    .subscribe(function(suggestion) {
    	appendToList("#wikipediasuggestionsOpt1", suggestion);
    });
    
    // Having done that optimisation, another immediately suggests itself: what if the 
    // final subscribe clause(s) were consolidated and then "promoted" into the body of the final
    // stream function? 

    // second 'optimised' variant:
    Stream.prototype.latestQueryResults = function(queryFn) {
    	var streamOfQueryResults = new Stream();
    	
		var self = this;
		var queryCallback = null;
		// The 'queryEvent' is the input event from the text box the user types into
		this.subscribe(s = function(queryEvent) {
			// Execute the query and subscribe to its stream of query results
			var queryStream = new Stream.query(queryEvent.currentTarget.value, queryFn);
			// if this was not the first queryEvent to arrive, stop tracking the previous 
			// ones; and remember this one for when its time comes
			if (queryCallback) {
				queryStream.unsubscribe(queryCallback);
			}
			queryCallback = function(suggestions){
				streamOfQueryResults._push(obj);
				$("#wikipediasuggestionsOpt2").empty();
				suggestions.forEach(function(suggestion) {
					appendToList("#wikipediasuggestionsOpt2", suggestion);
				});
			};
			queryStream.subscribe(queryCallback);
		});
		return streamOfQueryResults;
	}

    new Stream.dom($("#wikipediasearch"), "input")
    .throttle(1000)
    .latestQueryResults(WIKIPEDIAGET);

});
