var antlr4 = require('antlr4/index');
var HandlebarsLexer = require('HandlebarsLexer').HandlebarsLexer;
var HandlebarsParser = require('HandlebarsParser').HandlebarsParser;
var HandlebarsParserListener = require('HandlebarsParserListener').HandlebarsParserListener;

function HandlebarsCompiler() {
    HandlebarsParserListener.call(this);
    this._inputVar = "__$ctx";
    this._outputVar = "__$result";
    this._helpers = { expr: {}, block: {} };
    this._usedHelpers = new Set();
    
    this.registerBlockHelper("each", function (ctx, body, collection) {

    	var result = '';
    	for (let item of collection) {
    		result += body(item);
    	}
    	return result;
    });
    
    this.registerBlockHelper("if", function (ctx, body, expr) {
    	return (expr) ? body(ctx) : "";
    });
    
    this.registerBlockHelper("with", function (ctx, body, field) {
    	return body(ctx[field]);
    });
    
    return this;
}

HandlebarsCompiler.prototype = Object.create(HandlebarsParserListener.prototype);
HandlebarsCompiler.prototype.constructor = HandlebarsCompiler;

HandlebarsCompiler.escape = function (string) {
    return ('' + string).replace(/["'\\\n\r\u2028\u2029]/g, function (c) {
        switch (c) {
            case '"':
            case "'":
            case '\\':
                return '\\' + c;
            case '\n':
                return '\\n';
            case '\r':
                return '\\r';
            case '\u2028':
                return '\\u2028';
            case '\u2029':
                return '\\u2029';
        }
    })
};

HandlebarsCompiler.prototype.pushScope = function() {
    var se = `var ${this._outputVar} = '';`;
	se += "\n";
    
    this._bodyStack.push(se);
}

HandlebarsCompiler.prototype.popScope = function() {
	var tos = this._bodyStack.pop();
    tos += `\nreturn ${this._outputVar};\n`;
    
    return tos;
}

HandlebarsCompiler.prototype.appendCode = function (text) {
	this._bodyStack[this._bodyStack.length-1] += text;
}

HandlebarsCompiler.prototype.appendToOutput = function (expr) {
    this.appendCode(`
    	${this._outputVar} += ${expr};
    	`);
}

HandlebarsCompiler.prototype.registerExprHelper = function(name, helper) {
    this._helpers.expr[name] = helper;
};

HandlebarsCompiler.prototype.registerBlockHelper = function (name, helper) {
    this._helpers.block[name] = helper;
};

HandlebarsCompiler.prototype.compile = function (template) {

    var chars = new antlr4.InputStream(template);
    var lexer = new HandlebarsLexer(chars);
    var tokens = new antlr4.CommonTokenStream(lexer);
    var parser = new HandlebarsParser(tokens);
    parser.buildParseTrees = true;
    var tree = parser.document();
    
    this._bodyStack = new Array();
    this.pushScope();
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);
	body = this.popScope();
    
    var fnText = ''
    for (let k of this._usedHelpers) {
    	var f = (this._helpers.expr[k] || this._helpers.block[k]);
    	fnText += (`
    		var __$${k} = ${f.toString()};
    		`);
    }
	fnText += body;

	return new Function(this._inputVar, fnText);
};

// Elements 

HandlebarsCompiler.prototype.exitRawElement = function (ctx) {
    this.appendToOutput(`"${HandlebarsCompiler.escape(ctx.getText())}"`);
};

HandlebarsCompiler.prototype.exitExpressionElement = function (ctx) {
	this.appendToOutput(`${ctx.expression().source}`);
}

HandlebarsCompiler.prototype.enterBlockElement = function (ctx) {
	if (ctx.blockID.text != ctx.blockCloseID.text) {
		throw new Error(`Block start '${ctx.blockID.text}' does not match the block end '${ctx.blockCloseID.text}'.`);
	}	
}

HandlebarsCompiler.prototype.exitBlockElement = function (ctx) {
	
	var h = ctx.blockID.text;
	var f = this._helpers.block[h];
	if (!f) {
		throw new Error(`Undefined block helper function "${h}"`);
	}
	this._usedHelpers.add(h)

	var blockTemplate = ctx.blockBody().source;

		// call the helper with (first) ctx
	var fncall = `__$${h}(${this._inputVar}`;
		// second arg is the template function from the block body
	fncall += `, function(${this._inputVar}){ ${blockTemplate}; return ${this._outputVar}; }`;
		// and finally any arguments in the block header
	for (let arg of ctx.args) {
		fncall += `, ${arg.source}`;
	}
	fncall += `)`;

	this.appendToOutput(`${fncall}`);
}

HandlebarsCompiler.prototype.commentElement = function (ctx) {
	// do nothing
}

// Expressions

HandlebarsCompiler.prototype.exitExpression = function (ctx) {
	ctx.source = ctx.children[0].source;
}

HandlebarsCompiler.prototype.exitDatumExpr = function (ctx) {
	ctx.source = `${this._inputVar}['${ctx.getText()}']`;
};

HandlebarsCompiler.prototype.exitLiteralExpr = function (ctx) {
	ctx.source = `${ctx.getText()}`;
};

HandlebarsCompiler.prototype.exitHelperAppExpr = function (ctx) {

	var h = ctx.helper.text;
	var f = this._helpers.expr[h];
	if (!f) {
		throw new Error(`Undefined expression helper function "${h}"`);
	}
	this._usedHelpers.add(h)

	var fncall = `__$${h}(${this._inputVar}`;
	for (let arg of ctx.args) {
		fncall += `, ${arg.source}`;
	}
	fncall += `)`;
	ctx.source = fncall;
};

HandlebarsCompiler.prototype.exitParenExpr = function (ctx) {
	ctx.source = ctx.expression().source;
}

HandlebarsCompiler.prototype.exitBlockOpenExpression = function (ctx) {
	ctx.source = ctx.children[0].source;
}

HandlebarsCompiler.prototype.enterBlockBody = function (ctx) {
	this.pushScope();
}

HandlebarsCompiler.prototype.exitBlockBody = function (ctx) {
	ctx.source = this.popScope();
}

exports.HandlebarsCompiler = HandlebarsCompiler;
