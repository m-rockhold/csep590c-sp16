parser grammar HandlebarsParser;

options { language=JavaScript; tokenVocab=HandlebarsLexer; }

document: element* EOF ;

element
    : rawElement
    | expressionElement
    | blockElement
    | commentElement
    ;

rawElement
	: TEXT
	| BRACE TEXT
	;

commentElement : START COMMENT END_COMMENT ;

expressionElement
	: START expression END
	;

datumExpr returns [source]
	: ID
	;

literalExpr returns [source]
	: FLOAT
	| INTEGER
	| STRING
	;

helperAppExpr returns [source]
	: helper=ID (args += expression)+
	;

parenExpr returns [source]
	:	OPEN_PAREN expression CLOSE_PAREN
	;

expression returns [source]
	: literalExpr
	| datumExpr
	| helperAppExpr
	| parenExpr
	;

blockElement
	: START BLOCK blockID=ID (args += blockOpenExpression)* END blockBody START CLOSE_BLOCK blockCloseID=ID END
	;
	
blockOpenExpression returns [source]
	: literalExpr
	| datumExpr
	| parenExpr
	;
	
blockBody returns [source]
	: element*
	;
