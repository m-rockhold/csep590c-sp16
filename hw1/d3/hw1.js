!function() {
  var hw1 = {
    version: "1.0"
  };

  function elementsOverlap(sel1, sel2) {
        // bounding rectangles
        var n1 = sel1.node();
        var n2 = sel2.node();
        
        try {
            var r1 = n1.getBoundingClientRect();
            var r2 = n2.getBoundingClientRect();
            return (r1.left <= r2.right &&  r2.left <= r1.right && r1.top <= r2.bottom && r2.top <= r1.bottom);
        }
        catch (e) {
            console.log("exception: " + e + "; " + n1 + "; " + n2); // something's wrong with join() I think; sometimes getBoundingClientRect is not a function
            return false;
        }
    }

    // log circles
    function logOverlappingLabel(d) {
        var cr = this.getBoundingClientRect();
        console.log("Label for %s in (%d, %d, %d, %d) is obscured", d.name, cr.left, cr.right, cr.top, cr.bottom);
    }
    
    function logOutOfBoundsCircle(d) {
        var cr = this.getBoundingClientRect();
        console.log("Circle for %s in (%d, %d, %d, %d) is out of the frame", d.name, cr.left, cr.right, cr.top, cr.bottom);
    }
    
    function negate(pred) {
        return function() {
            return !pred.apply(this, arguments);
        };
    }

  function loadFilteredData(langdata) {

    var fyear = function(lang) { return lang.year; }
    var frepos = function(lang) { return lang.nbRepos; }

    var xScale = d3.scale.linear()
        .domain([d3.min(langdata, fyear)-1, d3.max(langdata, fyear)+2])
        .range([0, hw1.w]);

    var yScale = d3.scale.log()
        .domain([d3.min(langdata, frepos), d3.max(langdata, frepos)])
        .range([hw1.h, 0]);

    // X axis   
    hw1.xg.transition().duration(1500).call(d3.svg.axis()
        .scale(xScale)
        .orient("bottom")
        .ticks(16)
        .tickFormat(d3.format("4d")));

    // Y axis
    hw1.yg.transition().duration(1500).call(d3.svg.axis()
        .scale(yScale)
        .orient("left")
        .tickFormat(yScale.tickFormat(4, "6,g")));

    // Create the circles
    var circleSelection = hw1.drawing
        .selectAll("circle")
        .data(langdata, function(d) { return d.name; });

    circleSelection
        .transition().delay(500).duration(500)
        .attr("cx", function(lang) { return xScale(lang.year); })
        .attr("cy", function(lang) { return yScale(lang.nbRepos); })
        .callOnEndAll(function() {
                circleSelection
                .join(hw1.drawing, negate(elementsOverlap))
                .each(logOutOfBoundsCircle)
            });

    circleSelection
        .enter()
        .append("circle")
        .attr("cx", function(lang) { return xScale(lang.year); })
        .attr("cy", function(lang) { return yScale(lang.nbRepos); })
        .attr("r", 5)
        .style("opacity", "0")
        .transition().delay(1000).duration(500)
        .style("opacity", "1")
        .callOnEndAll(function() {
                circleSelection
                .join(hw1.drawing, negate(elementsOverlap))
                .each(logOutOfBoundsCircle)
            });

                  
    circleSelection.exit().transition().duration(500).style("opacity", "0").remove();

    // Similarly for the labels
    var labelSelection = hw1.drawing
        .selectAll(".label")
        .data(langdata, function(d) { return d.name + "_label"; });

    labelSelection
        .transition().delay(500).duration(500)
        .attr("x", function(lang) { return xScale(lang.year) + 6; })
        .attr("y", function(lang) { return yScale(lang.nbRepos) + 6; })
        .callOnEndAll(function() {
                labelSelection
                .join(hw1.drawing.selectAll("circle"),  elementsOverlap)
                .each(logOverlappingLabel)
            });

    labelSelection.exit().transition().duration(500).style("opacity", "0").remove();

    labelSelection
        .enter()
        .append("text")
        .attr("class", "label")
        .text(function(lang) { return lang.name; })
        .attr("x", function(lang) { return xScale(lang.year) + 6; })
        .attr("y", function(lang) { return yScale(lang.nbRepos) + 6; })
        .attr("font-family", "sans-serif")
        .attr("font-size", "11px")
        .style("opacity", "0")
        .transition().delay(1000).duration(500)
        .style("opacity", "1")
        .callOnEndAll(function() {
                labelSelection
                .join(hw1.drawing.selectAll("circle"),  elementsOverlap)
                .each(logOverlappingLabel)
            });
   }

hw1.load = function(dataName, height, width) {
    
    hw1.h = height;
    hw1.w = width;

    var svgContainer = d3.select("#problem2").append("svg")
        .attr("width", hw1.w+100)
        .attr("height", hw1.h+10+40);

    hw1.drawing = svgContainer.append("g")
        .attr("transform", "translate(60,10)");

    // X axis   
    hw1.xg = svgContainer.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(60," + (10+hw1.h+10) + ")");
    // Y axis
    hw1.yg = svgContainer.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(60,10)");
    
    d3.json(dataName, function(data) {
        var allLangData = data;
        var paradigms = [];

        function contains(list, element) {
            return list.indexOf(element) != -1;
        }

        function filterFor(paradigm, data) {
            if (paradigm === "All") {
                return data;
            }
            else {
                return data.filter(function(lang) {
                    return contains(lang.paradigms, paradigm);
                });
            }
        }

        data.forEach(function(lang, lindex) {
            lang.paradigms.forEach(function(paradigm, pindex) {
                if (!contains(paradigms, paradigm)) {
                    paradigms.push(paradigm);
                }
            });
        });
        paradigms.sort();
        paradigms.unshift("All");

        var select = d3.select("#problem2")
            .append("div")
            .append("select")
            .on("change", function() {
                loadFilteredData(filterFor(this.value, allLangData));
            });

        select.selectAll("option")
            .data(paradigms)
            .enter()
            .append("option")
            .attr("value", function(d) { return d; })
            .text(function(d) { return d; });

        loadFilteredData(allLangData);
    });
  }
  
  if (typeof define === "function" && define.amd) this.hw1 = hw1, define(hw1); else if (typeof module === "object" && module.exports) module.exports = hw1; else this.hw1 = hw1;
} ();

// Block on the last "end" event (from http://stackoverflow.com/a/20773846)
function endall(transition, callback) { 
    if (transition.size() === 0) { callback() }
    var n = 0; 
    transition
        .each(function() { ++n; }) 
        .each("end", function() { if (!--n) callback.apply(this, arguments); }); 
}

// Call a callback, blocking on the end of all transitions
d3.transition.prototype.callOnEndAll = function(callback) {
  return this.call(endall, callback);
};

d3.selection.prototype.join = function(sel2, pred) {
    //an element n in this will be in the result when there exists an element m in sel2 for which pred(n, m) is true.
    var result = this.filter(function(n, ni) {
        var _n = d3.select(this); // this is magical, we won't talk about it
        var trues = sel2.filter(function(m, mi) {
            return pred(_n, d3.select(this));
        });
        return trues[0].length > 0;
    });
    return result;
};
