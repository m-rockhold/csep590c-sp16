//
//  MRRegExp.swift
//  MRRegExp
//
//  Created by Rockhold, Michael on 4/10/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import Foundation

public struct MRRegExp {
    
    var prefixes = ""
    var suffixes = ""
    var flags = "m"
    var pattern = ""
    var captureCount = 0

    private static func sanitize(s: String) -> String {
        // TODO: return s.replace("/([].|*?+(){}^$\\:=[])/g", "\\$&")
        return s
    }
        
    public func asRegExp() -> NSRegularExpression? {
        let t = self.closeCaptures();
        do {
            return try NSRegularExpression(pattern: t.prefixes + t.pattern + t.suffixes, options:NSRegularExpressionOptions(rawValue:0));
        }
        catch _ {
            return nil;
        }
    }

    public func closeCaptures() -> MRRegExp {
        var t = self;
        while (t.captureCount > 0) {
            t = t.endCapture();
        }
        return t;
    }

    public func match(literal: String) -> MRRegExp {
        return self.then(literal);
    }

    public func then(pattern: String) -> MRRegExp {
        var copy = self
        copy.pattern += "(?:\(MRRegExp.sanitize(pattern)))"
        return copy
    }

    public func then(pattern: MRRegExp) -> MRRegExp {
        var copy = self
        copy.pattern += "(?:\(pattern))"
        return copy
    }

    public func startOfLine() -> MRRegExp {
        var copy = self
        copy.prefixes = "^" + self.prefixes;
        return copy
    }

    public func endOfLine() -> MRRegExp {
        var copy = self
        copy.suffixes = self.suffixes + "$";
        return copy
    }

    public func zeroOrMore() -> MRRegExp {
        var copy = self
        copy.pattern = "(?:\(self.pattern))*"
        return copy
    }

    public func zeroOrMore(pattern: MRRegExp) -> MRRegExp {
        return self.then(pattern.zeroOrMore());
    }

    public func oneOrMore() -> MRRegExp {
        var copy = self
        copy.pattern = "(?:\(self.pattern))+"
        return copy
    }

    public func oneOrMore(pattern: MRRegExp) -> MRRegExp {
        return self.then(pattern.oneOrMore());
    }

    public func optional() -> MRRegExp {
        var copy = self
        copy.pattern = "(?:\(self.pattern))?"
        return copy
    }

    public func maybe(pattern: String) -> MRRegExp {
        var copy = self
        copy.pattern += "(?:\(MRRegExp.sanitize(pattern)))?"
        return copy
    }

    public func maybe(pattern: MRRegExp) -> MRRegExp {
        var copy = self
        copy.pattern += "(?:\(pattern))?"
        return copy
    }

    public func anythingBut(characters: String) -> MRRegExp {
        var copy = self
        copy.pattern += "[^\(MRRegExp.sanitize(characters))]*"
        return copy
    }

    public func digit() -> MRRegExp {
        var copy = self
        copy.pattern += "\\d"
        return copy
    }

    public func repeated(from: Int) -> MRRegExp {
        var copy = self
        copy.pattern = "(?:\(self.pattern)){\(from),}"
        return copy
    }

    public func repeated(from: Int, to: Int) -> MRRegExp {
        var copy = self
        copy.pattern = "(?:\(self.pattern)){\(from),\(to)}"
        return copy
    }

    public func multiple(pattern: String, from: Int) -> MRRegExp {
        var copy = self
        copy.pattern += "(?:\(MRRegExp.sanitize(pattern))){\(from),}"
        return copy
    }

    public func multiple(pattern: MRRegExp, from: Int) -> MRRegExp {
        var copy = self
        copy.pattern += "(?:\(pattern)){\(from),}"
        return copy
    }

    public func multiple(pattern: String, from: Int, to: Int) -> MRRegExp {
        var copy = self
        copy.pattern += "(?:\(MRRegExp.sanitize(pattern))){\(from),\(to)}"
        return copy
    }

    public func multiple(pattern: MRRegExp, from: Int, to: Int) -> MRRegExp {
        var copy = self
        copy.pattern += "(?:\(pattern))){\(from),\(to)}"
        return copy
    }

    public func or(pattern: String) -> MRRegExp {
        var copy = self
        copy.pattern = "(?:\(self.pattern))|(?:\(MRRegExp.sanitize(pattern)))"
        return copy
    }

    public func or(pattern: MRRegExp) -> MRRegExp {
        var copy = self
        copy.pattern = "(?:\(self.pattern))|(?:\(pattern))"
        return copy
    }

    public func beginCapture() -> MRRegExp {
        var copy = self
        copy.pattern += "\\("
        copy.captureCount += 1
        return copy
    }

    public func endCapture() -> MRRegExp {
        var copy = self
        copy.pattern += "\\)"
        copy.captureCount -= 1
        return copy
    }
}

extension String {
    public init(regexp:MRRegExp) {
        self.init("\(regexp.prefixes)\(regexp.pattern)\(regexp.suffixes)")
    }
}