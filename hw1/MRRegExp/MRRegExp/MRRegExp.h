//
//  MRRegExp.h
//  MRRegExp
//
//  Created by Rockhold, Michael on 4/9/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for MRRegExp.
FOUNDATION_EXPORT double MRRegExpVersionNumber;

//! Project version string for MRRegExp.
FOUNDATION_EXPORT const unsigned char MRRegExpVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MRRegExp/PublicHeader.h>


