//
//  MRRegExpTests.swift
//  MRRegExpTests
//
//  Created by Rockhold, Michael on 4/9/16.
//  Copyright © 2016 Appel-Rockhold. All rights reserved.
//

import XCTest
@testable import MRRegExp

class MRRegExpTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSimple() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let oe = MRRegExp()
            .startOfLine()
            .then("http")
            .maybe("s")
            .then("://")
            .maybe("www.")
            .anythingBut(" ")
            .endOfLine()
            .asRegExp()

        if let e = oe {
            XCTAssert(e.numberOfMatchesInString("https://www.google.com/maps", options: NSMatchingOptions(rawValue:0), range: NSMakeRange(0, 27)) == 1)
        }
        else {
            XCTFail()
        }
    }

    func testCaptureNestedGroups() {
        
        let oe = MRRegExp()
            .startOfLine()
            .then("http")
            .maybe("s")
            .then("://")
            .maybe("www.")
            .beginCapture()
            .beginCapture()
            .anythingBut("/")
            .endCapture()
            .anythingBut(" ")
            .endCapture()
            .endOfLine()
            .asRegExp();
        
        if let e = oe {
            let matches = e.matchesInString("https://www.google.com/maps",
                                            options: NSMatchingOptions(rawValue:0),
                                            range: NSMakeRange(0, 27))
            
            if matches.count != 2 {
                XCTFail("matches.count != 2")
            }
            else {
                // NSTextCheckingResult
                //        expect(result[1]).to.be.equal("google.com/maps");
                //        expect(result[2]).to.be.equal("google.com");

            }
        }
        else {
            XCTFail()
        }
    }

}
